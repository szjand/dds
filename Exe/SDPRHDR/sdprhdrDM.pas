unit sdprhdrDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure tmpSDPRHDR;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'SDPRHDR';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\DDS\sunday\copy\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.tmpSDPRHDR;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'tmpSDPRHDR';
  dObject := 'tmpSDPRHDR';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpSDPRHDR');
          PrepareQuery(AdsQuery, 'insert into tmpSDPRHDR (' +
              'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, ' +
              'PTARCK, PTPMTH, PTDATE, PTCDAT, PTFCDT, PTVIN, PTTEST, PTSVCO, ' +
              'PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, PTCHK#, ' +
              'PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST,  ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, PTAPDT, ' +
              'PTDTCM, PTDTPI, PTTIIN, PTCREATE, PTDSPTEAM) ' +
              'values(' +
              ':PTCO, :PTRO, :PTRTYP, :PTWRO, :PTSWID, :PTRTCH, :PTCKEY, :PTCNAM, ' +
              ':PTARCK, :PTPMTH, :PTDATE, :PTCDAT, :PTFCDT, :PTVIN, :PTTEST, :PTSVCO, ' +
              ':PTSVCO2, :PTDEDA, :PTDEDA2, :PTWDED, :PTFRAN, :PTODOM, :PTMILO, :PTCHK, ' +
              ':PTPO, :PTREC, :PTPTOT, :PTLTOT, :PTSTOT, :PTDEDP, :PTSVCT, :PTSPOD, ' +
              ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
              ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, ' +
              ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
              ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTAUTH, :PTTAG, :PTAPDT, ' +
              ':PTDTCM, :PTDTPI, :PTTIIN, :PTCREATE, :PTDSPTEAM)');
            OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH,' +
                'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH, ' +
                'case ' +
                '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
                'end as PTDATE, ' +
                'case ' +
                '  when PTCDAT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) ' +
                'end as PTCDAT, ' +
                'case ' +
                '  when PTFCDT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) ' +
                'end as PTFCDT, ' +
                'PTVIN, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, ' +
                'PTCHK#, PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, PTCPHZ, ' +
                'PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, PTWAST3, PTWAST4, PTINST, ' +
                'PTINST2, PTINST3, PTINST4, PTSCST, PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, ' +
                'PTINSS, PTSCSS, PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, ' +
                'case ' +
                '  when PTAPDT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTAPDT), 4) || ''-'' || substr(digits(PTAPDT), 5, 2) || ''-'' || substr(digits(PTAPDT), 7, 2) as date) ' +
                'end as PTAPDT,  ' +
                'case ' +
                '  when PTDTCM = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDTCM), 4) || ''-'' || substr(digits(PTDTCM), 5, 2) || ''-'' || substr(digits(PTDTCM), 7, 2) as date) ' +
                'end as PTDTCM, ' +
                'case ' +
                '  when PTDTPI = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDTPI), 4) || ''-'' || substr(digits(PTDTPI), 5, 2) || ''-'' || substr(digits(PTDTPI), 7, 2) as date) ' +
                'end as PTDTPI, ' +
                'PTTIIN, ' +
                'case ' +
                '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
                '  else PTCREATE ' +
                'end as PTCREATE, ' +
                'PTDSPTEAM ' +
                'from rydedata.sdprhdr ' +
                'where trim(ptro#) in ( ' +
                '  select distinct trim(ptro#) ' +
                '  from rydedata.sdprhdr ' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(PTRO#) <> '''' ' +
                '    AND ( ' +
                '      CAST(LEFT(DIGITS(PTDATE), 4) || ''-'' || SUBSTR(DIGITS(PTDATE), 5, 2) || ''-'' || SUBSTR(DIGITS(PTDATE), 7, 2) AS DATE) > CURDATE() - 45 days ' +
                '      OR (PTCDAT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTCDAT), 4) || ''-'' || SUBSTR(DIGITS(PTCDAT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTCDAT), 7, 2) AS DATE) > CURDATE() - 45 days) ' +
                '      OR (PTFCDT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTFCDT), 4) || ''-'' || SUBSTR(DIGITS(PTFCDT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTFCDT), 7, 2) AS DATE) > CURDATE() - 45 days)) ' +
                '  union ' +
                '  select distinct trim(ptro#) ' +
                '  from rydedata.sdprdet ' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(ptro#) <> '''' ' +
                '    and ptdate <> 0 ' +
                '    and CAST(LEFT(DIGITS(ptdate), 4) || ''-'' || SUBSTR(DIGITS(ptdate), 5, 2) || ''-'' || SUBSTR(DIGITS(ptdate), 7, 2) AS DATE) > CURDATE() - 45 days)');
//                'where ptco# in (''RY1'',''RY2'') ' +
//                '  and length(trim(ptro#)) > 6 ' +
//                'and trim(ptro#) <> '''' ' +
//                'and (  ' +
//                '  cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//                '  or ( ' +
//                '    ptcdat <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//                '  or (' +
//                '    ptfcdt <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days))');
            while not AdoQuery.eof do
            begin
              AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
              AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
              AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
              AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
              AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
              AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
              AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
              AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
              AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
              AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
              AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
              AdsQuery.ParamByName('ptcdat').AsDate := AdoQuery.FieldByName('ptcdat').AsDateTime;
              AdsQuery.ParamByName('ptfcdt').AsDate := AdoQuery.FieldByName('ptfcdt').AsDateTime;
              AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
              AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
              AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
              AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
              AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
              AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
              AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
              AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
              AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
              AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
              AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
              AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
              AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
              AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
              AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
              AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
              AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
              AdsQuery.ParamByName('ptsvct').AsCurrency := AdoQuery.FieldByName('ptsvct').AsCurrency;
              AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
              AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
              AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
              AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
              AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
              AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
              AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
              AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
              AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
              AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
              AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
              AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
              AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
              AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
              AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
              AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
              AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
              AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
              AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
              AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
              AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
              AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
              AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
              AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
              AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
              AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
              AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
              AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
              AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
              AdsQuery.ParamByName('ptapdt').AsDate := AdoQuery.FieldByName('ptapdt').AsDateTime;
              AdsQuery.ParamByName('ptdtcm').AsDate := AdoQuery.FieldByName('ptdtcm').AsDateTime;
              AdsQuery.ParamByName('ptdtpi').AsDate := AdoQuery.FieldByName('ptdtpi').AsDateTime;
              AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
              AdsQuery.ParamByName('ptcreate').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('ptcreate').AsDateTime);
              AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
              AdsQuery.ExecSQL;
              AdoQuery.Next;
            end;
//          ScrapeCountQuery(' rydedata.sdprhdr ' +
//                'where ptco# in (''RY1'',''RY2'') ' +
//                'and length(trim(ptro#)) > 6 ' +
//                'and trim(ptro#) <> '''' ' +
//                'and (  ' +
//                '  cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//                '  or ( ' +
//                '    ptcdat <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//                '  or (' +
//                '    ptfcdt <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days))',
//                ' tmpSDPRHDR');
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpSDPRHDR'')');
//          ExecuteQuery(AdsQuery, 'execute procedure stgArkSDPRHDR()');
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


