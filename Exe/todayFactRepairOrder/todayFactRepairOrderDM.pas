unit todayFactRepairOrderDM;
(*)
  minimal dependency checking on dims
10/12
  need to add dimVehicle, dimCustomer
(**)
interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure todayClearRealTimeBatch;
    procedure todayPDPPHDR;
    procedure todayPDPPDET;
    procedure todaySDPRHDR;
    procedure todaySDPRDET;
    procedure todayINPMAST;
    procedure todayBOPNAME;
    procedure checkRoStatus;
    procedure todayFactRepairOrder;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'todayFactRepairOrder';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=Arkona Production';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;


procedure TDM.todayClearRealTimeBatch;
begin
  ExecuteQuery(AdsQuery, 'execute procedure todayClearRealTimeBatch()');
end;


procedure TDM.todayPDPPHDR;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todayPDPPHDR';
  dObject := 'todayPDPPHDR';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todayPDPPHDR');
          PrepareQuery(AdsQuery, ' insert into todayPDPPHDR(' +
              'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, PTDATE, PTDOC#, ' +
              'PTCUS#, PTCKEY, PTCNAM, PTPHON, PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
              'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
              'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
              'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
              'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
              'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
              'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
              'PTDTAR, PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
              'PTPRTY, PTRNT#, PTTIIN, PTCREATE, PTCTHOLD, PTAUTH#, PTAUTHID, ' +
              'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO) ' +
              'values(' +
              ':PTCO, :PTPKEY, :PTCPID, :PTSTAT, :PTDTYP, :PTTTYP, :PTDATE, :PTDOC, ' +
              ':PTCUS, :PTCKEY, :PTCNAM, :PTPHON, :PTSKEY, :PTPMTH, :PTSTYP, :PTPLVL, ' +
              ':PTTAXE, :PTPTOT, :PTSHPT, :PTSPOD, :PTSPDC, :PTSPDP, :PTSPDO, :PTSPOH, ' +
              ':PTSTAX, :PTSTAX2, :PTSTAX3, :PTSTAX4, :PTCPNM, :PTSRTK, :PTACTP, :PTCCAR, ' +
              ':PTPO, :PTREC, :PTODOC, :PTINTA, :PTRTYP, :PTRSTS, :PTWARO, :PTPAPRV, ' +
              ':PTSAPRV, :PTSWID, :PTRTCH, :PTTEST, :PTSVCO, :PTSVCO2, :PTDEDA, :PTDEDA2, ' +
              ':PTWDED, :PTVIN, :PTSTK, :PTTRCK, :PTFRAN, :PTODOM, :PTMILO, :PTRTIM, ' +
              ':PTTAG, :PTCHK, :PTHCMT, :PTSSOR, :PTHMOR, :PTLTOT, :PTSTOT, :PTDEDP, ' +
              ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
              ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, '  +
              ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
              ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTCPTD, :PTOSTS, ' +
              ':PTDTAR, :PTARCK, :PTWRO, :PTWSID, :PTAUTH, :PTAPDT, :PTDTLC, :PTDTPI, :PTIPTY, ' +
              ':PTPRTY, :PTRNT, :PTTIIN, :PTCREATE, :PTCTHOLD, :PTAUTH1, :PTAUTHID, ' +
              ':PTDLVMTH, :PTSHIPSEQ, :PTDSPTEAM, :PTTAXGO)');
          OpenQuery(AdoQuery, 'select ' +
              'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, ' +
              'cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) as PTDATE, ' +
              'PTDOC#, ' +
              'PTCUS#, PTCKEY, PTCNAM, ' +
              'CASE  WHEN PTPHON < 0 then 0 else ptphon end as ptphon, ' +
              'PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
              'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
              'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
              'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
              'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
              'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
              'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
              'case ' +
              '  when PTDTAR = 0 then cast(''9999-12-31'' as date) ' +
              '  else cast(left(digits(PTDTAR), 4) || ''-'' || substr(digits(PTDTAR), 5, 2) || ''-'' || substr(digits(PTDTAR), 7, 2) as date) ' +
              'end as PTDTAR, ' +
              'PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
              'PTPRTY, PTRNT#, PTTIIN, ' +
              'case ' +
              '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
              '  else PTCREATE ' +
              'end as PTCREATE, ' +
              'case ' +
              '  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
              '  else PTCTHOLD ' +
              'end as PTCTHOLD, ' +
              'PTAUTH#, PTAUTHID, ' +
              'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO ' +
              'from rydedata.pdpphdr where ptpkey in (' +
                'select distinct ptpkey ' +
                'from rydedata.pdpphdr ' +
                'where ptdtyp = ''RO'' and ptdoc# <> '''' ' +
                '  and ' +
                '    case  ' +
                '      when ptdate = 0 then cast(''9999-12-31'' as date) ' +
                '      else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) '+
                '    end = curdate() ' +
                'union ' +
                'select distinct a.ptpkey ' +
                'from rydedata.pdppdet a ' +
                'inner join rydedata.pdpphdr b on a.ptpkey = b.ptpkey ' +
                '  and b.ptdtyp = ''RO'' and b.ptdoc# <> '''' ' +
                'where ' +
                '    case  ' +
                '      when a.ptdate = 0 then cast(''9999-12-31'' as date) ' +
                '      else cast(left(digits(a.PTDATE), 4) || ''-'' || substr(digits(a.PTDATE), 5, 2) || ''-'' || substr(digits(a.PTDATE), 7, 2) as date) ' +
                '    end = curdate())');
//              'where ptdtyp = ''RO'' and ptdoc# <> '''' ' +
//              'and cast(left(digits(a.PTDATE), 4) || ''-'' || substr(digits(a.PTDATE), 5, 2) || ''-'' || substr(digits(a.PTDATE), 7, 2) as date) = curdate()');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
            AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
            AdsQuery.ParamByName('ptstat').AsString := AdoQuery.FieldByName('ptstat').AsString;
            AdsQuery.ParamByName('ptdtyp').AsString := AdoQuery.FieldByName('ptdtyp').AsString;
            AdsQuery.ParamByName('ptttyp').AsString := AdoQuery.FieldByName('ptttyp').AsString;
            AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptdoc').AsString := AdoQuery.FieldByName('ptdoc#').AsString;
            AdsQuery.ParamByName('ptcus').AsString := AdoQuery.FieldByName('ptcus#').AsString;
            AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
            AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
            AdsQuery.ParamByName('ptphon').AsString := AdoQuery.FieldByName('ptphon').AsString;
            AdsQuery.ParamByName('ptskey').AsInteger := AdoQuery.FieldByName('ptskey').AsInteger;
            AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
            AdsQuery.ParamByName('ptstyp').AsString := AdoQuery.FieldByName('ptstyp').AsString;
            AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
            AdsQuery.ParamByName('pttaxe').AsString := AdoQuery.FieldByName('pttaxe').AsString;
            AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
            AdsQuery.ParamByName('ptshpt').AsCurrency := AdoQuery.FieldByName('ptshpt').AsCurrency;
            AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
            AdsQuery.ParamByName('ptspdc').AsCurrency := AdoQuery.FieldByName('ptspdc').AsCurrency;
            AdsQuery.ParamByName('ptspdp').AsCurrency := AdoQuery.FieldByName('ptspdp').AsCurrency;
            AdsQuery.ParamByName('ptspdo').AsString := AdoQuery.FieldByName('ptspdo').AsString;
            AdsQuery.ParamByName('ptspoh').AsString := AdoQuery.FieldByName('ptspoh').AsString;
            AdsQuery.ParamByName('ptstax').AsCurrency := AdoQuery.FieldByName('ptstax').AsCurrency;
            AdsQuery.ParamByName('ptstax2').AsCurrency := AdoQuery.FieldByName('ptstax2').AsCurrency;
            AdsQuery.ParamByName('ptstax3').AsCurrency := AdoQuery.FieldByName('ptstax3').AsCurrency;
            AdsQuery.ParamByName('ptstax4').AsCurrency := AdoQuery.FieldByName('ptstax4').AsCurrency;
            AdsQuery.ParamByName('ptcpnm').AsString := AdoQuery.FieldByName('ptcpnm').AsString;
            AdsQuery.ParamByName('ptsrtk').AsString := AdoQuery.FieldByName('ptsrtk').AsString;
            AdsQuery.ParamByName('ptactp').AsString := AdoQuery.FieldByName('ptactp').AsString;
            AdsQuery.ParamByName('ptccar').AsString := AdoQuery.FieldByName('ptccar').AsString;
            AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
            AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
            AdsQuery.ParamByName('ptodoc').AsString := AdoQuery.FieldByName('ptodoc').AsString;
            AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
            AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
            AdsQuery.ParamByName('ptrsts').AsString := AdoQuery.FieldByName('ptrsts').AsString;
            AdsQuery.ParamByName('ptwaro').AsString := AdoQuery.FieldByName('ptwaro').AsString;
            AdsQuery.ParamByName('ptpaprv').AsString := AdoQuery.FieldByName('ptpaprv').AsString;
            AdsQuery.ParamByName('ptsaprv').AsString := AdoQuery.FieldByName('ptsaprv').AsString;
            AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
            AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
            AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
            AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
            AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
            AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
            AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
            AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
            AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
            AdsQuery.ParamByName('ptstk').AsString := AdoQuery.FieldByName('ptstk#').AsString;
            AdsQuery.ParamByName('pttrck').AsString := AdoQuery.FieldByName('pttrck').AsString;
            AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
            AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
            AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
            AdsQuery.ParamByName('ptrtim').AsFloat := AdoQuery.FieldByName('ptrtim').AsFloat;
            AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
            AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
            AdsQuery.ParamByName('pthcmt').AsString := AdoQuery.FieldByName('pthcmt').AsString;
            AdsQuery.ParamByName('ptssor').AsString := AdoQuery.FieldByName('ptssor').AsString;
            AdsQuery.ParamByName('pthmor').AsString := AdoQuery.FieldByName('pthmor').AsString;
            AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
            AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
            AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
            AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
            AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
            AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
            AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
            AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
            AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
            AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
            AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
            AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
            AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
            AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
            AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
            AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
            AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
            AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
            AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
            AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
            AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
            AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
            AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
            AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
            AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
            AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
            AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
            AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
            AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
            AdsQuery.ParamByName('ptcptd').AsCurrency := AdoQuery.FieldByName('ptcptd').AsCurrency;
            AdsQuery.ParamByName('ptosts').AsString := AdoQuery.FieldByName('ptosts').AsString;
            AdsQuery.ParamByName('ptdtar').AsDateTime := AdoQuery.FieldByName('ptdtar').AsDateTime;
            AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
            AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
            AdsQuery.ParamByName('ptwsid').AsString := AdoQuery.FieldByName('ptwsid').AsString;
            AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
            AdsQuery.ParamByName('ptapdt').AsInteger := AdoQuery.FieldByName('ptapdt').AsInteger;
            AdsQuery.ParamByName('ptdtlc').AsInteger := AdoQuery.FieldByName('ptdtlc').AsInteger;
            AdsQuery.ParamByName('ptdtpi').AsInteger := AdoQuery.FieldByName('ptdtpi').AsInteger;
            AdsQuery.ParamByName('ptipty').AsString := AdoQuery.FieldByName('ptipty').AsString;
            AdsQuery.ParamByName('ptprty').AsString := AdoQuery.FieldByName('ptprty').AsString;
            AdsQuery.ParamByName('ptrnt').AsInteger := AdoQuery.FieldByName('ptrnt#').AsInteger;
            AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
            AdsQuery.ParamByName('ptcreate').AsDateTime := AdoQuery.FieldByName('ptcreate').AsDateTime;
            AdsQuery.ParamByName('ptcthold').AsDateTime := AdoQuery.FieldByName('ptcthold').AsDateTime;
            AdsQuery.ParamByName('ptauth1').AsString := AdoQuery.FieldByName('ptauth#').AsString;
            AdsQuery.ParamByName('ptauthid').AsString := AdoQuery.FieldByName('ptauthid').AsString;
            AdsQuery.ParamByName('ptdlvmth').AsInteger := AdoQuery.FieldByName('ptdlvmth').AsInteger;
            AdsQuery.ParamByName('ptshipseq').AsInteger := AdoQuery.FieldByName('ptshipseq').AsInteger;
            AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
            AdsQuery.ParamByName('pttaxgo').AsInteger := AdoQuery.FieldByName('pttaxgo').AsInteger;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todayPDPPHDR'')');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.todayPDPPDET;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todayPDPPDET';
  dObject := 'todayPDPPDET';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todayPDPPDET');
          PrepareQuery(AdsQuery, ' insert into todayPDPPDET(' +
              'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
              'PTDATE, ' +
              'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
              'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
              'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
              'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
              'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
              'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
              'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
              'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
              'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
              'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME) ' +
              'values(' +
              ':PTCO, :PTPKEY, :PTLINE, :PTLTYP, :PTSEQ, :PTCPID, :PTCODE, :PTSOEP, ' +
              ':PTDATE, :PTMANF, :PTPART, :PTPLNG, :PTSGRP, :PTBINL, :PTQTY, :PTCOST, ' +
              ':PTLIST, :PTTRAD, :PTFPRC, :PTNET, :PTBLIST, :PTMETH, :PTPCT, :PTBASE, ' +
              ':PTSPCD, :PTPOVR, :PTGPRC, :PTCORE, :PTADDP, :PTSPPC, :PTORSO, :PTXCLD, ' +
              ':PTCMNT, :PTLSTS, :PTSVCTYP, :PTLPYM, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, ' +
              ':PTLHRS, :PTLCHR, :PTARLA, :PTFAIL, :PTPLVL, :PTPSAS, :PTSLV, :PTSLI, ' +
              ':PTDCPN, :PTFACT, :PTACODE, :PTBAWC, :PTDTLC, :PTINTA, :PTLAUTH, :PTNTOT, ' +
              ':PTOAMT, :PTOHRS, :PTPMCS, :PTPMRT, :PTRPRP, :PTTXIN, :PTUPSL, :PTWCTP, ' +
              ':PTVATCODE, :PTVATAMT, :PTRTNINVF, :PTSKLLVL, :PTOEMRPLF, :PTCSTDIF, ' +
              ':PTOPTSEQ, :PTDISSTRZ, :PTDSCLBR, :PTDSCPRT, :PTPICKZNE, :PTDISPTY1, ' +
              ':PTDISPTY2, :PTDISRANK1, :PTDISRANK2, :PTDSPADATE, :PTDSPATIME, :PTDSPMDATE, :PTDSPMTIME)');
          OpenQuery(AdoQuery, 'select ' +
              'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
              'case ' +
              '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
              '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
              'end as PTDATE, ' +
              'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
              'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
              'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
              'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
              'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
              'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
              'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
              'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
              'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
              'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME ' +
              'from rydedata.pdppdet ' +
              'where ptpkey in ( ' +
                'select distinct ptpkey ' +
                'from rydedata.pdpphdr ' +
                'where ptdtyp = ''RO'' and ptdoc# <> '''' ' +
                '  and ' +
                '    case  ' +
                '      when ptdate = 0 then cast(''9999-12-31'' as date) ' +
                '      else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) '+
                '    end = curdate() ' +
                'union ' +
                'select distinct a.ptpkey ' +
                'from rydedata.pdppdet a ' +
                'inner join rydedata.pdpphdr b on a.ptpkey = b.ptpkey ' +
                '  and b.ptdtyp = ''RO'' and b.ptdoc# <> '''' ' +
                'where ' +
                '    case  ' +
                '      when a.ptdate = 0 then cast(''9999-12-31'' as date) ' +
                '      else cast(left(digits(a.PTDATE), 4) || ''-'' || substr(digits(a.PTDATE), 5, 2) || ''-'' || substr(digits(a.PTDATE), 7, 2) as date) ' +
                '    end = curdate())');
//              'where case when b.ptdate = 0 then cast(''9999-12-31'' as date) else ' +
//              'cast(left(digits(b.PTDATE), 4) || ''-'' || substr(digits(b.PTDATE), 5, 2) || ''-'' || substr(digits(b.PTDATE), 7, 2) as date) ' +
//              'end = curdate() ' +
//              'or ptpkey in (select ptpkey from rydedata.pdpphdr a WHERE a.ptdtyp = ''RO'' ' +
//              'AND a.ptdoc# <> '''' ' +
//              'AND cast(left(digits(a.PTDATE), 4) || ''-'' || substr(digits(a.PTDATE), 5, 2) || ''-'' || substr(digits(a.PTDATE), 7, 2) as date) = curdate())');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
            AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
            AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
            AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
            AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
            AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
            AdsQuery.ParamByName('ptsoep').AsString := AdoQuery.FieldByName('ptsoep').AsString;
            AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptmanf').AsString := AdoQuery.FieldByName('ptmanf').AsString;
            AdsQuery.ParamByName('ptpart').AsString := AdoQuery.FieldByName('ptpart').AsString;
            AdsQuery.ParamByName('ptplng').AsInteger := AdoQuery.FieldByName('ptplng').AsInteger;
            AdsQuery.ParamByName('ptsgrp').AsString := AdoQuery.FieldByName('ptsgrp').AsString;
            AdsQuery.ParamByName('ptbinl').AsString := AdoQuery.FieldByName('ptbinl').AsString;
            AdsQuery.ParamByName('ptqty').AsInteger := AdoQuery.FieldByName('ptqty').AsInteger;
            AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
            AdsQuery.ParamByName('ptlist').AsCurrency := AdoQuery.FieldByName('ptlist').AsCurrency;
            AdsQuery.ParamByName('pttrad').AsCurrency := AdoQuery.FieldByName('pttrad').AsCurrency;
            AdsQuery.ParamByName('ptfprc').AsCurrency := AdoQuery.FieldByName('ptfprc').AsCurrency;
            AdsQuery.ParamByName('ptnet').AsCurrency := AdoQuery.FieldByName('ptnet').AsCurrency;
            AdsQuery.ParamByName('ptblist').AsCurrency := AdoQuery.FieldByName('ptblist').AsCurrency;
            AdsQuery.ParamByName('ptmeth').AsString := AdoQuery.FieldByName('ptmeth').AsString;
            AdsQuery.ParamByName('ptpct').AsInteger := AdoQuery.FieldByName('ptpct').AsInteger;
            AdsQuery.ParamByName('ptbase').AsString := AdoQuery.FieldByName('ptbase').AsString;
            AdsQuery.ParamByName('ptspcd').AsString := AdoQuery.FieldByName('ptspcd').AsString;
            AdsQuery.ParamByName('ptpovr').AsString := AdoQuery.FieldByName('ptpovr').AsString;
            AdsQuery.ParamByName('ptgprc').AsString := AdoQuery.FieldByName('ptgprc').AsString;
            AdsQuery.ParamByName('ptcore').AsString := AdoQuery.FieldByName('ptcore').AsString;
            AdsQuery.ParamByName('ptaddp').AsString := AdoQuery.FieldByName('ptaddp').AsString;
            AdsQuery.ParamByName('ptsppc').AsInteger := AdoQuery.FieldByName('ptsppc').AsInteger;
            AdsQuery.ParamByName('ptorso').AsString := AdoQuery.FieldByName('ptorso').AsString;
            AdsQuery.ParamByName('ptxcld').AsString := AdoQuery.FieldByName('ptxcld').AsString;
            AdsQuery.ParamByName('ptcmnt').AsString := AdoQuery.FieldByName('ptcmnt').AsString;
            AdsQuery.ParamByName('ptlsts').AsString := AdoQuery.FieldByName('ptlsts').AsString;
            AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
            AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
            AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
            AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
            AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
            AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
            AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
            AdsQuery.ParamByName('ptlchr').AsFloat := AdoQuery.FieldByName('ptlchr').AsFloat;
            AdsQuery.ParamByName('ptarla').AsCurrency := AdoQuery.FieldByName('ptarla').AsCurrency;
            AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
            AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
            AdsQuery.ParamByName('ptpsas').AsInteger := AdoQuery.FieldByName('ptpsas').AsInteger;
            AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
            AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
            AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
            AdsQuery.ParamByName('ptfact').AsFloat := AdoQuery.FieldByName('ptfact').AsFloat;
            AdsQuery.ParamByName('ptacode').AsString := AdoQuery.FieldByName('ptacode').AsString;
            AdsQuery.ParamByName('ptbawc').AsString := AdoQuery.FieldByName('ptbawc').AsString;
            AdsQuery.ParamByName('ptdtlc').AsFloat := AdoQuery.FieldByName('ptdtlc').AsFloat;
            AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
            AdsQuery.ParamByName('ptlauth').AsString := AdoQuery.FieldByName('ptlauth').AsString;
            AdsQuery.ParamByName('ptntot').AsFloat := AdoQuery.FieldByName('ptntot').AsFloat;
            AdsQuery.ParamByName('ptoamt').AsFloat := AdoQuery.FieldByName('ptoamt').AsFloat;
            AdsQuery.ParamByName('ptohrs').AsFloat := AdoQuery.FieldByName('ptohrs').AsFloat;
            AdsQuery.ParamByName('ptpmcs').AsFloat := AdoQuery.FieldByName('ptpmcs').AsFloat;
            AdsQuery.ParamByName('ptpmrt').AsFloat := AdoQuery.FieldByName('ptpmrt').AsFloat;
            AdsQuery.ParamByName('ptrprp').AsString := AdoQuery.FieldByName('ptrprp').AsString;
            AdsQuery.ParamByName('pttxin').AsString := AdoQuery.FieldByName('pttxin').AsString;
            AdsQuery.ParamByName('ptupsl').AsString := AdoQuery.FieldByName('ptupsl').AsString;
            AdsQuery.ParamByName('ptwctp').AsString := AdoQuery.FieldByName('ptwctp').AsString;
            AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
            AdsQuery.ParamByName('ptvatamt').AsFloat := AdoQuery.FieldByName('ptvatamt').AsFloat;
            AdsQuery.ParamByName('ptrtninvf').AsString := AdoQuery.FieldByName('ptrtninvf').AsString;
            AdsQuery.ParamByName('ptskllvl').AsString := AdoQuery.FieldByName('ptskllvl').AsString;
            AdsQuery.ParamByName('ptoemrplf').AsString := AdoQuery.FieldByName('ptoemrplf').AsString;
            AdsQuery.ParamByName('ptcstdif').AsFloat := AdoQuery.FieldByName('ptcstdif$').AsFloat;
            AdsQuery.ParamByName('ptoptseq').AsInteger := AdoQuery.FieldByName('ptoptseq#').AsInteger;
            AdsQuery.ParamByName('ptdisstrz').AsString := ''; //AdoQuery.FieldByName('ptdisstrz').AsString;
            AdsQuery.ParamByName('ptdsclbr').AsFloat := AdoQuery.FieldByName('ptdsclbr$').AsFloat;
            AdsQuery.ParamByName('ptdscprt').AsFloat := AdoQuery.FieldByName('ptdscprt$').AsFloat;
            AdsQuery.ParamByName('ptpickzne').AsString := AdoQuery.FieldByName('ptpickzne').AsString;
            AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
            AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
            AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
            AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
            AdsQuery.ParamByName('ptdspadate').AsString := ''; //AdoQuery.FieldByName('ptdspadate').AsString;
            AdsQuery.ParamByName('ptdspatime').AsString := ''; //AdoQuery.FieldByName('ptdspatime').AsString;
            AdsQuery.ParamByName('ptdspmdate').AsString := ''; //AdoQuery.FieldByName('ptdspmdate').AsString;
            AdsQuery.ParamByName('ptdspmtime').AsString := ''; //AdoQuery.FieldByName('ptdspmtime').AsString;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todayPDPPDET'')');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.todaySDPRHDR;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todaySDPRHDR';
  dObject := 'todaySDPRHDR';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todaySDPRHDR');
          PrepareQuery(AdsQuery, 'insert into todaySDPRHDR (' +
              'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, ' +
              'PTARCK, PTPMTH, PTDATE, PTCDAT, PTFCDT, PTVIN, PTTEST, PTSVCO, ' +
              'PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, PTCHK#, ' +
              'PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST,  ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, PTAPDT, ' +
              'PTDTCM, PTDTPI, PTTIIN, PTCREATE, PTDSPTEAM) ' +
              'values(' +
              ':PTCO, :PTRO, :PTRTYP, :PTWRO, :PTSWID, :PTRTCH, :PTCKEY, :PTCNAM, ' +
              ':PTARCK, :PTPMTH, :PTDATE, :PTCDAT, :PTFCDT, :PTVIN, :PTTEST, :PTSVCO, ' +
              ':PTSVCO2, :PTDEDA, :PTDEDA2, :PTWDED, :PTFRAN, :PTODOM, :PTMILO, :PTCHK, ' +
              ':PTPO, :PTREC, :PTPTOT, :PTLTOT, :PTSTOT, :PTDEDP, :PTSVCT, :PTSPOD, ' +
              ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
              ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, ' +
              ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
              ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTAUTH, :PTTAG, :PTAPDT, ' +
              ':PTDTCM, :PTDTPI, :PTTIIN, :PTCREATE, :PTDSPTEAM)');
            OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH,' +
                'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH, ' +
                'case ' +
                '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
                'end as PTDATE, ' +
                'case ' +
                '  when PTCDAT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) ' +
                'end as PTCDAT, ' +
                'case ' +
                '  when PTFCDT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) ' +
                'end as PTFCDT, ' +
                'PTVIN, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, ' +
                'PTCHK#, PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, PTCPHZ, ' +
                'PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, PTWAST3, PTWAST4, PTINST, ' +
                'PTINST2, PTINST3, PTINST4, PTSCST, PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, ' +
                'PTINSS, PTSCSS, PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, ' +
                'case ' +
                '  when PTAPDT = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTAPDT), 4) || ''-'' || substr(digits(PTAPDT), 5, 2) || ''-'' || substr(digits(PTAPDT), 7, 2) as date) ' +
                'end as PTAPDT,  ' +
                'case ' +
                '  when PTDTCM = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDTCM), 4) || ''-'' || substr(digits(PTDTCM), 5, 2) || ''-'' || substr(digits(PTDTCM), 7, 2) as date) ' +
                'end as PTDTCM, ' +
                'case ' +
                '  when PTDTPI = 0 then cast(''9999-12-31'' as date) ' +
                '  else cast(left(digits(PTDTPI), 4) || ''-'' || substr(digits(PTDTPI), 5, 2) || ''-'' || substr(digits(PTDTPI), 7, 2) as date) ' +
                'end as PTDTPI, ' +
                'PTTIIN, ' +
                'case ' +
                '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
                '  else PTCREATE ' +
                'end as PTCREATE, ' +
                'PTDSPTEAM ' +
                'from rydedata.sdprhdr where trim(ptro#) in ( ' +
                '  select distinct trim(ptro#)' +
                '  from rydedata.sdprhdr' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(PTRO#) <> ''''' +
                '    AND ( ' +
                '      CAST(LEFT(DIGITS(PTDATE), 4) || ''-'' || SUBSTR(DIGITS(PTDATE), 5, 2) || ''-'' || SUBSTR(DIGITS(PTDATE), 7, 2) AS DATE) = CURDATE() ' +
                '      OR (PTCDAT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTCDAT), 4) || ''-'' || SUBSTR(DIGITS(PTCDAT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTCDAT), 7, 2) AS DATE) = CURDATE()) ' +
                '      OR (PTFCDT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTFCDT), 4) || ''-'' || SUBSTR(DIGITS(PTFCDT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTFCDT), 7, 2) AS DATE) = CURDATE())) ' +
                '  union ' +
                '  select distinct trim(ptro#) ' +
                '  from rydedata.sdprdet ' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(ptro#) <> '''' ' +
                '    and ptdate <> 0 ' +
                '    and CAST(LEFT(DIGITS(ptdate), 4) || ''-'' || SUBSTR(DIGITS(ptdate), 5, 2) || ''-'' || SUBSTR(DIGITS(ptdate), 7, 2) AS DATE) = CURDATE())');
//                'where ptco# in (''RY1'',''RY2'') ' +
//                'and trim(ptro#) <> '''' ' +
//                'and (  ' +
//                '  cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) = curdate() ' +
//                '  or ( ' +
//                '    ptcdat <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) = curdate()) ' +
//                '  or (' +
//                '    ptfcdt <> 0 ' +
//                '    and  ' +
//                '    cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) = curdate()))');
            while not AdoQuery.eof do
            begin
              AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
              AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
              AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
              AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
              AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
              AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
              AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
              AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
              AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
              AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
              AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
              AdsQuery.ParamByName('ptcdat').AsDate := AdoQuery.FieldByName('ptcdat').AsDateTime;
              AdsQuery.ParamByName('ptfcdt').AsDate := AdoQuery.FieldByName('ptfcdt').AsDateTime;
              AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
              AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
              AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
              AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
              AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
              AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
              AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
              AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
              AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
              AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
              AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
              AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
              AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
              AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
              AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
              AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
              AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
              AdsQuery.ParamByName('ptsvct').AsCurrency := AdoQuery.FieldByName('ptsvct').AsCurrency;
              AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
              AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
              AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
              AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
              AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
              AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
              AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
              AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
              AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
              AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
              AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
              AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
              AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
              AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
              AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
              AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
              AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
              AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
              AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
              AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
              AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
              AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
              AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
              AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
              AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
              AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
              AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
              AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
              AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
              AdsQuery.ParamByName('ptapdt').AsDate := AdoQuery.FieldByName('ptapdt').AsDateTime;
              AdsQuery.ParamByName('ptdtcm').AsDate := AdoQuery.FieldByName('ptdtcm').AsDateTime;
              AdsQuery.ParamByName('ptdtpi').AsDate := AdoQuery.FieldByName('ptdtpi').AsDateTime;
              AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
              AdsQuery.ParamByName('ptcreate').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('ptcreate').AsDateTime);
              AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
              AdsQuery.ExecSQL;
              AdoQuery.Next;
            end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todaySDPRHDR'')');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.todaySDPRDET;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todaySDPRDET';
  dObject := 'todaySDPRDET';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todaySDPRDET');
          PrepareQuery(AdsQuery, 'insert into todaySDPRDET values(' +
              ':PTCO, :PTRO, :PTLINE, :PTLTYP, :PTSEQ, :PTCODE, :PTDATE, :PTLPYM, ' +
              ':PTSVCTYP, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, :PTLHRS, :PTLAMT, ' +
              ':PTCOST, :PTARTF, :PTFAIL, :PTSLV, :PTSLI, :PTDCPN, :PTDBAS, ' +
              ':PTVATCODE, :PTVATAMT, :PTDISPTY1, :PTDISPTY2, :PTDISRANK1, ' +
              ':PTDISRANK2)');
          OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, ' +
              ' case ' +
              '   when ptdate = 0 then cast(''9999-12-31'' as date) ' +
              ' else ' +
              '   cast(left(digits(ptdate), 4) || ''-'' || substr(digits(ptdate), 5, 2) || ''-'' || substr(digits(ptdate), 7, 2) as date) ' +
              ' end as PTDATE, ' +
              ' PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF, ' +
              ' PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2, ' +
              ' PTDISRANK1, PTDISRANK2 ' +
              ' from RYDEDATA.SDPRDET where trim(ptro#) in ( ' +
                '  select distinct trim(ptro#)' +
                '  from rydedata.sdprhdr' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(PTRO#) <> ''''' +
                '    AND ( ' +
                '      CAST(LEFT(DIGITS(PTDATE), 4) || ''-'' || SUBSTR(DIGITS(PTDATE), 5, 2) || ''-'' || SUBSTR(DIGITS(PTDATE), 7, 2) AS DATE) = CURDATE() ' +
                '      OR (PTCDAT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTCDAT), 4) || ''-'' || SUBSTR(DIGITS(PTCDAT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTCDAT), 7, 2) AS DATE) = CURDATE()) ' +
                '      OR (PTFCDT <> 0 AND ' +
                '        CAST(LEFT(DIGITS(PTFCDT), 4) || ''-'' || SUBSTR(DIGITS(PTFCDT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTFCDT), 7, 2) AS DATE) = CURDATE())) ' +
                '  union ' +
                '  select distinct trim(ptro#) ' +
                '  from rydedata.sdprdet ' +
                '  where ptco# in (''RY1'',''RY2'') AND TRIM(ptro#) <> '''' ' +
                '    and ptdate <> 0 ' +
                '    and CAST(LEFT(DIGITS(ptdate), 4) || ''-'' || SUBSTR(DIGITS(ptdate), 5, 2) || ''-'' || SUBSTR(DIGITS(ptdate), 7, 2) AS DATE) = CURDATE())');
//              ' where ptco# in (''RY1'', ''RY2'') ' + // and ptdbas <> ''V'' ' +
//              '   and trim(ptro#) in (' +
//              '     select trim(ptro#) from rydedata.sdprhdr ' +
//              '     where ptco# in (''RY1'',''RY2'') ' +
//              '       and trim(ptro#) <> '''' ' +
//              '       and (  ' +
//              '         cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) = curdate() ' +
//              '         or ( ' +
//              '           ptcdat <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) = curdate()) ' +
//              '         or (' +
//              '           ptfcdt <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) = curdate())))');
          while not AdoQuery.eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
            AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
            AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
            AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
            AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
            if  AdoQuery.FieldByName('ptdate').AsString = '12/31/9999' then
              AdsQuery.ParamByName('ptdate').Clear
            else
              AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
            AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
            AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
            AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
            AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
            AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
            AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
            AdsQuery.ParamByName('ptlamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
            AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
            AdsQuery.ParamByName('ptartf').AsString := AdoQuery.FieldByName('ptartf').AsString;
            AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
            AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
            AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
            AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
            AdsQuery.ParamByName('ptdbas').AsString := AdoQuery.FieldByName('ptdbas').AsString;
            AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
            AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
            AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
            AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
            AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
            AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todaySDPRDET'')');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.todayINPMAST;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todayINPMAST';
  dObject := 'dimVehicle';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todayINPMAST');
          PrepareQuery(AdsQuery, ' insert into todayINPMAST(' +
              'IMCO#, IMVIN, IMSTK#, IMDOC#, IMSTAT, IMGTRN, IMTYPE, IMFRAN, ' +
              'IMYEAR, IMMAKE, IMMCODE, IMMODL, IMBODY, IMCOLR, IMODOM, IMDINV, ' +
              'IMDSVC, IMDDLV, IMDORD, IMSACT, IMIACT, IMLIC#, IMODOMA, IMKEY, IMPRIC, IMCOST) ' +
              'values(' +
              ':IMCO, :IMVIN, :IMSTK, :IMDOC, :IMSTAT, :IMGTRN, :IMTYPE, :IMFRAN, ' +
              ':IMYEAR, :IMMAKE, :IMMCODE, :IMMODL, :IMBODY, :IMCOLR, :IMODOM, :IMDINV, ' +
              ':IMDSVC, :IMDDLV, :IMDORD, :IMSACT, :IMIACT, :IMLIC, :IMODOMA, :IMKEY, :IMPRIC, :IMCOST)');
          OpenQuery(AdoQuery, 'select ' +
              '  imco#, imvin, imstk#, imdoc#, imstat, ' +
              '  imgtrn, imtype, imfran, imyear, immake, ' +
              '  immcode, immodl, imbody, imcolr, imodom, ' +
              '  case ' +
              '    when imdinv = 0 or imdinv = 10101 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdinv), 4) || ''-'' || substr(digits(imdinv), 5, 2) || ''-'' || substr(digits(imdinv), 7, 2) as date) ' +
              '  end as imdinv, ' +
              '  case ' +
              '    when imdsvc = 0 or imdsvc = 10101 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdsvc), 4) || ''-'' || substr(digits(imdsvc), 5, 2) || ''-'' || substr(digits(imdsvc), 7, 2) as date)  ' +
              '  end as imdsvc, ' +
              '  case ' +
              '    when imddlv < 19000000 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imddlv), 4) || ''-'' || substr(digits(imddlv), 5, 2) || ''-'' || substr(digits(imddlv), 7, 2) as date) ' +
              '  end as imddlv, ' +
              '  case ' +
              '    when imdord = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdord), 4) || ''-'' || substr(digits(imdord), 5, 2) || ''-'' || substr(digits(imdord), 7, 2) as date) ' +
              '  end as imdord, ' +
              '  imsact, imiact, imlic#, imodoma, imkey, impric, imcost ' +
              'from rydedata.inpmast ' +
              'where imco# = ''RY1'' and trim(imvin) in (' +
              '  select trim(ptvin)  ' +
              '  from rydedata.pdpphdr ' +
              '  where ptdtyp = ''RO'' and ptdoc# <> '''' ' +
              '  and cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) = curdate() ' +
              '  union ' +
              '  select trim(ptvin) ' +
              '  from rydedata.sdprhdr ' +
              '  where ptco# in (''RY1'',''RY2'') ' +
              '  and trim(ptro#) <> '''' ' +
              '  and ( ' +
              '    cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) = curdate() ' +
              '    or ( ' +
              '      ptcdat <> 0 ' +
              '      and ' +
              '      cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) = curdate()) ' +
              '    or ( ' +
              '      ptfcdt <> 0  ' +
              '      and ' +
              '      cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) = curdate())))');
//              '      cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) = curdate()))) order by imvin');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('imco').AsString := AdoQuery.FieldByName('imco#').AsString;
            AdsQuery.ParamByName('imvin').AsString := AdoQuery.FieldByName('imvin').AsString;
            AdsQuery.ParamByName('imstk').AsString := AdoQuery.FieldByName('imstk#').AsString;
            AdsQuery.ParamByName('imdoc').AsString := AdoQuery.FieldByName('imdoc#').AsString;
            AdsQuery.ParamByName('imstat').AsString := AdoQuery.FieldByName('imstat').AsString;
            AdsQuery.ParamByName('imgtrn').AsString := AdoQuery.FieldByName('imgtrn').AsString;
            AdsQuery.ParamByName('imtype').AsString := AdoQuery.FieldByName('imtype').AsString;
            AdsQuery.ParamByName('imfran').AsString := AdoQuery.FieldByName('imfran').AsString;
            AdsQuery.ParamByName('imyear').AsInteger := AdoQuery.FieldByName('imyear').AsInteger;
            AdsQuery.ParamByName('immake').AsString := AdoQuery.FieldByName('immake').AsString;
            AdsQuery.ParamByName('immcode').AsString := AdoQuery.FieldByName('immcode').AsString;
            AdsQuery.ParamByName('immodl').AsString := AdoQuery.FieldByName('immodl').AsString;
            AdsQuery.ParamByName('imbody').AsString := AdoQuery.FieldByName('imbody').AsString;
            AdsQuery.ParamByName('imcolr').AsString := AdoQuery.FieldByName('imcolr').AsString;
            AdsQuery.ParamByName('imodom').AsInteger := AdoQuery.FieldByName('imodom').AsInteger;
            AdsQuery.ParamByName('imdinv').AsDateTime := AdoQuery.FieldByName('imdinv').AsDateTime;
            AdsQuery.ParamByName('imdsvc').AsDateTime := AdoQuery.FieldByName('imdsvc').AsDateTime;
            AdsQuery.ParamByName('imddlv').AsDateTime := AdoQuery.FieldByName('imddlv').AsDateTime;
            AdsQuery.ParamByName('imdord').AsDateTime := AdoQuery.FieldByName('imdord').AsDateTime;
            AdsQuery.ParamByName('imsact').AsString := AdoQuery.FieldByName('imsact').AsString;
            AdsQuery.ParamByName('imiact').AsString := AdoQuery.FieldByName('imiact').AsString;
            AdsQuery.ParamByName('imlic').AsString := AdoQuery.FieldByName('imlic#').AsString;
            AdsQuery.ParamByName('imodoma').AsString := AdoQuery.FieldByName('imodoma').AsString;
            AdsQuery.ParamByName('imkey').AsInteger := AdoQuery.FieldByName('imkey').AsInteger;
            AdsQuery.ParamByName('impric').AsCurrency := AdoQuery.FieldByName('impric').AsCurrency;
            AdsQuery.ParamByName('imcost').AsCurrency := AdoQuery.FieldByName('imcost').AsCurrency;
//
//            writeln(AdoQuery.FieldByName('imvin').AsString);
//
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todayINPMAST'')');
          ExecuteQuery(AdsQuery, 'execute procedure todayXfmDimVehicle()');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDM.todayBOPNAME;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todayBOPNAME';
  dObject := 'dimCustomer';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from todayBOPNAME');
          PrepareQuery(AdsQuery, ' insert into todayBOPNAME(' +
              'BNCO#, BNKEY, BNTYPE, BNSS#, BNSNAM, BNLNAM, BNFNAM, BNMIDI, ' +
              'BNSALU, BNGENDER, BNLANG, BNADR1, BNADR2, BNCITY, BNCNTY, BNSTCD, ' +
              'BNZIP, BNPHON, BNBPHN, BNBEXT, BNFAX#, BNBDAT, BNDLIC, BNCNTC, ' +
              'BNPCNTC, BNMAIL, BNTAXE, BNCSTS, BNCTYPE, BNPREFPHN, BNCPHON, ' +
              'BNPPHON, BNOPHON, BNOPDESC, BNEMAIL, BNOPTF, BNACBM, BNACBP, ' +
              'BNADBE, BNADR3, BNBEXTA, BNBPHNI, BNCDAT, BNCPHONI, BNEKEY, ' +
              'BNEML2, BNFAX#I, BNOPHONI, BNPHONI, BNPNAM, BNPPHONI, BNPRLN, ' +
              'BNZIPI, BNTOKEN, BNCCID, BNSXFNAM, BNSXLNAM, BNSXADR1, BNOPTF2, ' +
              'BNTAXE2, BNTAXE3, BNTAXE4, BNGSTREG#, BNPSTDAT, BNDLRCD, BNCCCD, ' +
              'BNCSTVER, BNADRVER, BNADRVCUR, BNCRTUSR, BNCRTTS, BNUPDUSR, ' +
              'BNUPDTS, BNCTRYCD, BNDLST, BNOTPTRCD, BNSTATUS) ' +
              'values(' +
              ':BNCO, :BNKEY, :BNTYPE, :BNSS, :BNSNAM, :BNLNAM, :BNFNAM, :BNMIDI, ' +
              ':BNSALU, :BNGENDER, :BNLANG, :BNADR1, :BNADR2, :BNCITY, :BNCNTY, :BNSTCD, ' +
              ':BNZIP, :BNPHON, :BNBPHN, :BNBEXT, :BNFAX, :BNBDAT, :BNDLIC, :BNCNTC, ' +
              ':BNPCNTC, :BNMAIL, :BNTAXE, :BNCSTS, :BNCTYPE, :BNPREFPHN, :BNCPHON, ' +
              ':BNPPHON, :BNOPHON, :BNOPDESC, :BNEMAIL, :BNOPTF, :BNACBM, :BNACBP, ' +
              ':BNADBE, :BNADR3, :BNBEXTA, :BNBPHNI, :BNCDAT, :BNCPHONI, :BNEKEY, ' +
              ':BNEML2, :BNFAXI, :BNOPHONI, :BNPHONI, :BNPNAM, :BNPPHONI, :BNPRLN, ' +
              ':BNZIPI, :BNTOKEN, :BNCCID, :BNSXFNAM, :BNSXLNAM, :BNSXADR1, :BNOPTF2, ' +
              ':BNTAXE2, :BNTAXE3, :BNTAXE4, :BNGSTREG, :BNPSTDAT, :BNDLRCD, :BNCCCD, ' +
              ':BNCSTVER, :BNADRVER, :BNADRVCUR, :BNCRTUSR, :BNCRTTS, :BNUPDUSR, ' +
              ':BNUPDTS, :BNCTRYCD, :BNDLST, :BNOTPTRCD, :BNSTATUS)');
          OpenQuery(AdoQuery, 'select '  +
              'BNCO#, BNKEY, BNTYPE, BNSS#, BNSNAM, BNLNAM, BNFNAM, '  +
              'BNMIDI, BNSALU, BNGENDER, BNLANG, BNADR1, BNADR2, BNCITY, '  +
              'BNCNTY, BNSTCD, BNZIP, BNPHON, BNBPHN, BNBEXT, BNFAX#, '  +
              'case '  +
              '  when BNBDAT < 18000000 then cast(''9999-12-31'' as date) '  +
              '  else cast(left(digits(BNBDAT), 4) || ''-'' || substr(digits(BNBDAT), 5, 2) || ''-'' || substr(digits(BNBDAT), 7, 2) as date) '  +
              'end as BNBDAT, '  +
              'BNDLIC, BNCNTC, BNPCNTC, BNMAIL, BNTAXE, BNCSTS, '  +
              'BNCTYPE, BNPREFPHN, BNCPHON, BNPPHON, BNOPHON, BNOPDESC, '  +
              'BNEMAIL, BNOPTF, BNACBM, BNACBP, BNADBE, BNADR3, BNBEXTA, '  +
              'BNBPHNI, '  +
              'case '  +
              '  when BNCDAT < 18000000 then cast(''9999-12-31'' as date) '  +
              '  else cast(left(digits(BNCDAT), 4) || ''-'' || substr(digits(BNCDAT), 5, 2) || ''-'' || substr(digits(BNCDAT), 7, 2) as date) '  +
              'end as BNCDAT, '  +
              'BNCPHONI, BNEKEY, BNEML2, BNFAX#I, '  +
              'BNOPHONI, BNPHONI, BNPNAM, BNPPHONI, BNPRLN, BNZIPI, '  +
              'BNTOKEN, BNCCID, BNSXFNAM, BNSXLNAM, BNSXADR1, BNOPTF2, '  +
              'BNTAXE2, BNTAXE3, BNTAXE4, BNGSTREG#, BNPSTDAT, BNDLRCD, '  +
              'BNCCCD, BNCSTVER, BNADRVER, BNADRVCUR, BNCRTUSR, '  +
              'CHAR(BNCRTTS) as BNCRTTS, BNUPDUSR, CHAR(BNUPDTS) as BNUPDTS, BNCTRYCD, BNDLST, '  +
            'BNOTPTRCD, BNSTATUS '  +
            'from rydedata.bopname where date(bnupdts) = curdate()');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('bnco').AsString := AdoQuery.FieldByName('bnco#').AsString;
            AdsQuery.ParamByName('bnkey').AsInteger := AdoQuery.FieldByName('bnkey').AsInteger;
            AdsQuery.ParamByName('bntype').AsString := AdoQuery.FieldByName('bntype').AsString;
            AdsQuery.ParamByName('bnss').AsInteger := AdoQuery.FieldByName('bnss#').AsInteger;
            AdsQuery.ParamByName('bnsnam').AsString := AdoQuery.FieldByName('bnsnam').AsString;
            AdsQuery.ParamByName('bnlnam').AsString := AdoQuery.FieldByName('bnlnam').AsString;
            AdsQuery.ParamByName('bnfnam').AsString := AdoQuery.FieldByName('bnfnam').AsString;
            AdsQuery.ParamByName('bnmidi').AsString := AdoQuery.FieldByName('bnmidi').AsString;
            AdsQuery.ParamByName('bnsalu').AsString := AdoQuery.FieldByName('bnsalu').AsString;
            AdsQuery.ParamByName('bngender').AsString := AdoQuery.FieldByName('bngender').AsString;
            AdsQuery.ParamByName('bnlang').AsString := AdoQuery.FieldByName('bnlang').AsString;
            AdsQuery.ParamByName('bnadr1').AsString := AdoQuery.FieldByName('bnadr1').AsString;
            AdsQuery.ParamByName('bnadr2').AsString := AdoQuery.FieldByName('bnadr2').AsString;
            AdsQuery.ParamByName('bncity').AsString := AdoQuery.FieldByName('bncity').AsString;
            AdsQuery.ParamByName('bncnty').AsString := AdoQuery.FieldByName('bncnty').AsString;
            AdsQuery.ParamByName('bnstcd').AsString := AdoQuery.FieldByName('bnstcd').AsString;
            AdsQuery.ParamByName('bnzip').AsString := AdoQuery.FieldByName('bnzip').AsString;
            AdsQuery.ParamByName('bnphon').AsString := AdoQuery.FieldByName('bnphon').AsString;
            AdsQuery.ParamByName('bnbphn').AsString := AdoQuery.FieldByName('bnbphn').AsString;
            AdsQuery.ParamByName('bnbext').AsString := AdoQuery.FieldByName('bnbext').AsString;
            AdsQuery.ParamByName('bnfax').AsString := AdoQuery.FieldByName('bnfax#').AsString;
            AdsQuery.ParamByName('bnbdat').AsDateTime := AdoQuery.FieldByName('bnbdat').AsDateTime;
            AdsQuery.ParamByName('bndlic').AsString := AdoQuery.FieldByName('bndlic').AsString;
            AdsQuery.ParamByName('bncntc').AsString := AdoQuery.FieldByName('bncntc').AsString;
            AdsQuery.ParamByName('bnpcntc').AsString := AdoQuery.FieldByName('bnpcntc').AsString;
            AdsQuery.ParamByName('bnmail').AsString := AdoQuery.FieldByName('bnmail').AsString;
            AdsQuery.ParamByName('bntaxe').AsString := AdoQuery.FieldByName('bntaxe').AsString;
            AdsQuery.ParamByName('bncsts').AsString := AdoQuery.FieldByName('bncsts').AsString;
            AdsQuery.ParamByName('bnctype').AsString := AdoQuery.FieldByName('bnctype').AsString;
            AdsQuery.ParamByName('bnprefphn').AsString := AdoQuery.FieldByName('bnprefphn').AsString;
            AdsQuery.ParamByName('bncphon').AsString := AdoQuery.FieldByName('bncphon').AsString;
            AdsQuery.ParamByName('bnpphon').AsString := AdoQuery.FieldByName('bnpphon').AsString;
            AdsQuery.ParamByName('bnophon').AsString := AdoQuery.FieldByName('bnophon').AsString;
            AdsQuery.ParamByName('bnopdesc').AsString := AdoQuery.FieldByName('bnopdesc').AsString;
            AdsQuery.ParamByName('bnemail').AsString := AdoQuery.FieldByName('bnemail').AsString;
            AdsQuery.ParamByName('bnoptf').AsString := AdoQuery.FieldByName('bnoptf').AsString;
            AdsQuery.ParamByName('bnacbm').AsString := AdoQuery.FieldByName('bnacbm').AsString;
            AdsQuery.ParamByName('bnacbp').AsString := AdoQuery.FieldByName('bnacbp').AsString;
            AdsQuery.ParamByName('bnadbe').AsString := AdoQuery.FieldByName('bnadbe').AsString;
            AdsQuery.ParamByName('bnadr3').AsString := AdoQuery.FieldByName('bnadr3').AsString;
            AdsQuery.ParamByName('bnbexta').AsString := AdoQuery.FieldByName('bnbexta').AsString;
            AdsQuery.ParamByName('bnbphni').AsString := AdoQuery.FieldByName('bnbphni').AsString;
            AdsQuery.ParamByName('bncdat').AsDateTime := AdoQuery.FieldByName('bncdat').AsDateTime;
            AdsQuery.ParamByName('bncphoni').AsString := AdoQuery.FieldByName('bncphoni').AsString;
            AdsQuery.ParamByName('bnekey').AsInteger := AdoQuery.FieldByName('bnekey').AsInteger;
            AdsQuery.ParamByName('bneml2').AsString := AdoQuery.FieldByName('bneml2').AsString;
            AdsQuery.ParamByName('bnfaxi').AsString := AdoQuery.FieldByName('bnfax#i').AsString;
            AdsQuery.ParamByName('bnophoni').AsString := AdoQuery.FieldByName('bnophoni').AsString;
            AdsQuery.ParamByName('bnphoni').AsString := AdoQuery.FieldByName('bnphoni').AsString;
            AdsQuery.ParamByName('bnpnam').AsString := AdoQuery.FieldByName('bnpnam').AsString;
            AdsQuery.ParamByName('bnpphoni').AsString := AdoQuery.FieldByName('bnpphoni').AsString;
            AdsQuery.ParamByName('bnprln').AsString := AdoQuery.FieldByName('bnprln').AsString;
            AdsQuery.ParamByName('bnzipi').AsString := AdoQuery.FieldByName('bnzipi').AsString;
            AdsQuery.ParamByName('bntoken').AsString := AdoQuery.FieldByName('bntoken').AsString;
            AdsQuery.ParamByName('bnccid').AsString := AdoQuery.FieldByName('bnccid').AsString;
            AdsQuery.ParamByName('bnsxfnam').AsString := AdoQuery.FieldByName('bnsxfnam').AsString;
            AdsQuery.ParamByName('bnsxlnam').AsString := AdoQuery.FieldByName('bnsxlnam').AsString;
            AdsQuery.ParamByName('bnsxadr1').AsString := AdoQuery.FieldByName('bnsxadr1').AsString;
            AdsQuery.ParamByName('bnoptf2').AsString := AdoQuery.FieldByName('bnoptf2').AsString;
            AdsQuery.ParamByName('bntaxe2').AsString := AdoQuery.FieldByName('bntaxe2').AsString;
            AdsQuery.ParamByName('bntaxe3').AsString := AdoQuery.FieldByName('bntaxe3').AsString;
            AdsQuery.ParamByName('bntaxe4').AsString := AdoQuery.FieldByName('bntaxe4').AsString;
            AdsQuery.ParamByName('bngstreg').AsString := AdoQuery.FieldByName('bngstreg#').AsString;
            AdsQuery.ParamByName('bnpstdat').AsInteger := AdoQuery.FieldByName('bnpstdat').AsInteger;
            AdsQuery.ParamByName('bndlrcd').AsInteger := AdoQuery.FieldByName('bndlrcd').AsInteger;
            //Ccase 306
            AdsQuery.ParamByName('bncccd').AsFloat := 0;//AdoQuery.FieldByName('bncccd').AsFloat;
            AdsQuery.ParamByName('bncstver').AsInteger := AdoQuery.FieldByName('bncstver').AsInteger;
            //Ccase 306
            AdsQuery.ParamByName('bnadrver').AsInteger := 0;//AdoQuery.FieldByName('bnadrver').AsInteger;
            AdsQuery.ParamByName('bnadrvcur').AsInteger := AdoQuery.FieldByName('bnadrvcur').AsInteger;
            AdsQuery.ParamByName('bncrtusr').AsInteger := AdoQuery.FieldByName('bncrtusr').AsInteger;
            AdsQuery.ParamByName('bncrtts').AsString := AdoQuery.FieldByName('bncrtts').AsString;
            AdsQuery.ParamByName('bnupdusr').AsInteger := AdoQuery.FieldByName('bnupdusr').AsInteger;
            AdsQuery.ParamByName('bnupdts').AsString := AdoQuery.FieldByName('bnupdts').AsString;
            AdsQuery.ParamByName('bnctrycd').AsString := AdoQuery.FieldByName('bnctrycd').AsString;
            AdsQuery.ParamByName('bndlst').AsString := AdoQuery.FieldByName('bndlst').AsString;
            AdsQuery.ParamByName('bnotptrcd').AsString := AdoQuery.FieldByName('bnotptrcd').AsString;
            AdsQuery.ParamByName('bnstatus').AsString := AdoQuery.FieldByName('bnstatus').AsString;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''todayBOPNAME'')');
          ExecuteQuery(AdsQuery, 'execute procedure todayXfmDimCust()');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.checkRoStatus;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'checkRoStatus';
  dObject := 'dimRoStatus';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          OpenQuery(AdsQuery, 'execute procedure todayCheckRoStatus()');
          PassFail := AdsQuery.FieldByName('PassFail').AsString;
          CloseQuery(AdsQuery);
          if PassFail = 'Fail' then
            sendmail('*** todayCheckRoStatus Failed ***', 'as indicated by todayCheckRoStatus')
          else
            DM.ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + ' : MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.todayFactRepairOrder;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'todayFactRepairOrder';
  dObject := 'todayFactRepairOrder';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure todayCheckDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'execute procedure todayXfmFactRepairOrder()');
          ExecuteQuery(AdsQuery, 'execute procedure TodayUpdateRealTimeBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;



procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


