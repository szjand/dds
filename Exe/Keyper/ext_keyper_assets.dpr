program ext_keyper_assets;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  KeyperDM in 'KeyperDM.pas' {DM: TDataModule};

resourcestring
  executable = 'ext_keyper_assets';


begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
// *** active procs ***//
      DM.keyper_assets;
// *** active procs ***//
    except
      on E: Exception do
      begin
        DM.SendMail('ext_keyper_assets.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
