unit dimOpCodeDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure SDPLOPC;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'dimOpCode';

implementation

{$R *.dfm}

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;
procedure TDM.SDPLOPC;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'SDPLOPC';
  dObject := 'dimOpCode';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
{$region 'scrape and populate stgArkonaSDPLOPC}
            ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaSDPLOPC'')');
            PrepareQuery(AdsQuery, ' insert into stgArkonaSDPLOPC(' +
                'SOCO#, SOLOPC, SODES1, SODES2, SODES3, SODES4, SOMAJC, SOMINC, ' +
                'SOMANF, SOMAKE, SOMODL, SOENGN, SOYEAR, SODLPM, SOCORR, SOTYPE, ' +
                'SOXDIS, SOSKIL, SOTAXB, SOHMCHG, SOSUPC, SORMTH, SORAMT, SOCMTH, ' +
                'SOCAMT, SOPMTH, SOPAMT, SOESTM, SOEHRS, SOWAITF, SOCDS1, SODES5, ' +
                'SODES6, SOMDS1, SODFTSVCT)' +
                'values(' +
                ':SOCO, :SOLOPC, :SODES1, :SODES2, :SODES3, :SODES4, :SOMAJC, :SOMINC, ' +
                ':SOMANF, :SOMAKE, :SOMODL, :SOENGN, :SOYEAR, :SODLPM, :SOCORR, :SOTYPE, ' +
                ':SOXDIS, :SOSKIL, :SOTAXB, :SOHMCHG, :SOSUPC, :SORMTH, :SORAMT, :SOCMTH, ' +
                ':SOCAMT, :SOPMTH, :SOPAMT, :SOESTM, :SOEHRS, :SOWAITF, :SOCDS1, :SODES5, ' +
                ':SODES6, :SOMDS1, :SODFTSVCT)');
            OpenQuery(AdoQuery, 'select ' +
                'SOCO#, SOLOPC, SODES1, SODES2, SODES3, SODES4, SOMAJC, SOMINC, ' +
                'SOMANF, SOMAKE, SOMODL, SOENGN, SOYEAR, SODLPM, SOCORR, SOTYPE, ' +
                'SOXDIS, SOSKIL, SOTAXB, SOHMCHG, SOSUPC, SORMTH, SORAMT, SOCMTH, ' +
                'SOCAMT, SOPMTH, SOPAMT, SOESTM, SOEHRS, SOWAITF, SOCDS1, SODES5, ' +
                'SODES6, SOMDS1, SODFTSVCT ' +
                'from rydedata.sdplopc where soco# <> ''RY3''');
            while not AdoQuery.Eof do
            begin
              AdsQuery.ParamByName('soco').AsString := AdoQuery.FieldByName('soco#').AsString;
              AdsQuery.ParamByName('solopc').AsString := AdoQuery.FieldByName('solopc').AsString;
              AdsQuery.ParamByName('sodes1').AsString := AdoQuery.FieldByName('sodes1').AsString;
              AdsQuery.ParamByName('sodes2').AsString := AdoQuery.FieldByName('sodes2').AsString;
              AdsQuery.ParamByName('sodes3').AsString := AdoQuery.FieldByName('sodes3').AsString;
              AdsQuery.ParamByName('sodes4').AsString := AdoQuery.FieldByName('sodes4').AsString;
              AdsQuery.ParamByName('somajc').AsString := AdoQuery.FieldByName('somajc').AsString;
              AdsQuery.ParamByName('sominc').AsString := AdoQuery.FieldByName('sominc').AsString;
              AdsQuery.ParamByName('somanf').AsString := AdoQuery.FieldByName('somanf').AsString;
              AdsQuery.ParamByName('somake').AsString := AdoQuery.FieldByName('somake').AsString;
              AdsQuery.ParamByName('somodl').AsString := AdoQuery.FieldByName('somodl').AsString;
              AdsQuery.ParamByName('soengn').AsString := AdoQuery.FieldByName('soengn').AsString;
              AdsQuery.ParamByName('soyear').AsString := AdoQuery.FieldByName('soyear').AsString;
              AdsQuery.ParamByName('sodlpm').AsString := AdoQuery.FieldByName('sodlpm').AsString;
              AdsQuery.ParamByName('socorr').AsString := AdoQuery.FieldByName('socorr').AsString;
              AdsQuery.ParamByName('sotype').AsString := AdoQuery.FieldByName('sotype').AsString;
              AdsQuery.ParamByName('soxdis').AsString := AdoQuery.FieldByName('soxdis').AsString;
              AdsQuery.ParamByName('soskil').AsString := AdoQuery.FieldByName('soskil').AsString;
              AdsQuery.ParamByName('sotaxb').AsString := AdoQuery.FieldByName('sotaxb').AsString;
              AdsQuery.ParamByName('sohmchg').AsCurrency := AdoQuery.FieldByName('sohmchg').AsCurrency;
              AdsQuery.ParamByName('sosupc').AsString := AdoQuery.FieldByName('sosupc').AsString;
              AdsQuery.ParamByName('sormth').AsString := AdoQuery.FieldByName('sormth').AsString;
              AdsQuery.ParamByName('soramt').AsCurrency := AdoQuery.FieldByName('soramt').AsCurrency;
              AdsQuery.ParamByName('socmth').AsString := AdoQuery.FieldByName('socmth').AsString;
              AdsQuery.ParamByName('socamt').AsCurrency := AdoQuery.FieldByName('socamt').AsCurrency;
              AdsQuery.ParamByName('sopmth').AsString := AdoQuery.FieldByName('sopmth').AsString;
              AdsQuery.ParamByName('sopamt').AsCurrency := AdoQuery.FieldByName('sopamt').AsCurrency;
              AdsQuery.ParamByName('soestm').AsString := AdoQuery.FieldByName('soestm').AsString;
              AdsQuery.ParamByName('soehrs').AsFloat := AdoQuery.FieldByName('soehrs').AsFloat;
              AdsQuery.ParamByName('sowaitf').AsString := AdoQuery.FieldByName('sowaitf').AsString;
              AdsQuery.ParamByName('socds1').AsString := AdoQuery.FieldByName('socds1').AsString;
              AdsQuery.ParamByName('sodes5').AsString := AdoQuery.FieldByName('sodes5').AsString;
              AdsQuery.ParamByName('sodes6').AsString := AdoQuery.FieldByName('sodes6').AsString;
              AdsQuery.ParamByName('somds1').AsString := AdoQuery.FieldByName('somds1').AsString;
              AdsQuery.ParamByName('sodftsvct').AsString := AdoQuery.FieldByName('sodftsvct').AsString;
              AdsQuery.ExecSQL;
              AdoQuery.Next;
            end;
            ScrapeCountQuery(' rydedata.sdplopc where soco# <> ''RY3''', 'stgArkonaSDPLOPC');
            ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPLOPC'')');
{$endregion}
          ExecuteQuery(AdsQuery, 'execute procedure xfmOpCode()');
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('dimOpCode.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'utilities'}
//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;



procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}


end.
