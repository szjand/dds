unit EmpDimDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure PYPCLKCTL;
    procedure PYMAST;
    procedure PYACTGR;
    procedure xfmEmpDim;
    procedure maintDimEmployee();
    procedure PYPRHEAD;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'EmployeeDim';

implementation

{$R *.dfm}

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
// local
// ads.cartiva.com
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.PYPCLKCTL;
var
  proc: string;
begin
  proc := 'PYPCLKCTL';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpPYPCLKCTL'')');
      PrepareQuery(AdsQuery, 'insert into tmpPYPCLKCTL (' +
          'YICCO#, YICDEPT, YICDESC, YICDSW, YICSORT, YICAPP1, YICAPP2, YICAPP3, YICAPP4) ' +
          'values(' +
          ':YICCO, :YICDEPT, :YICDESC, :YICDSW, :YICSORT, :YICAPP1, :YICAPP2, :YICAPP3, :YICAPP4)');
      OpenQuery(AdoQuery, 'select yicco#, yicdept, yicdesc, yicdsw, yicsort, ' +
        'yicapp1, yicapp2, yicapp3, yicapp4 from rydedata.pypclkctl');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('yicco').AsString := AdoQuery.FieldByName('yicco#').AsString;
        AdsQuery.ParamByName('yicdept').AsString := AdoQuery.FieldByName('yicdept').AsString;
        AdsQuery.ParamByName('yicdesc').AsString := AdoQuery.FieldByName('yicdesc').AsString;
        AdsQuery.ParamByName('yicdsw').AsString := AdoQuery.FieldByName('yicdsw').AsString;
        AdsQuery.ParamByName('yicsort').AsString := AdoQuery.FieldByName('yicsort').AsString;
        AdsQuery.ParamByName('yicapp1').AsString := AdoQuery.FieldByName('yicapp1').AsString;
        AdsQuery.ParamByName('yicapp2').AsString := AdoQuery.FieldByName('yicapp2').AsString;
        AdsQuery.ParamByName('yicapp3').AsString := AdoQuery.FieldByName('yicapp3').AsString;
        AdsQuery.ParamByName('yicapp4').AsString := AdoQuery.FieldByName('yicapp4').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPYPCLKCTL'')');
      ScrapeCountTable('PYPCLKCTL', 'tmpPYPCLKCTL');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPYPCLKCTL()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPCLKCTL.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.PYPRHEAD;
var
  proc: string;
begin
  proc := 'PYPRHEAD';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYPRHEAD'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYPRHEAD(' +
          'YRCO#, YREMPN, YRMGRN, YRJOBD, YRJOBL, YRHDTE, YRHDTO, YRTDTE, ' +
          'YRRDTE, YRNDTE, YRCDTE, YRBP01, YRBP02, YRBP03, YRBP04, YRBP05, ' +
          'YRBP06, YRBP07, YRBP08, YRBP09, YRBP10, YRBP11, YRBP12, YRBP13, ' +
          'YRBP14, YRBP15, YRBP16, YRDP01, YRDP02, YRDP03, YRDP04, YRDP05, ' +
          'YRDP06, YRDP07, YRDP08, YRDP09, YRDP10, YRDP11, YRDP12, YRDP13, ' +
          'YRDP14, YRDP15, YRDP16) ' +
          'values(' +
          ':YRCO, :YREMPN, :YRMGRN, :YRJOBD, :YRJOBL, :YRHDTE, :YRHDTO, :YRTDTE, ' +
          ':YRRDTE, :YRNDTE, :YRCDTE, :YRBP01, :YRBP02, :YRBP03, :YRBP04, :YRBP05, :' +
          'YRBP06, :YRBP07, :YRBP08, :YRBP09, :YRBP10, :YRBP11, :YRBP12, :YRBP13, ' +
          ':YRBP14, :YRBP15, :YRBP16, :YRDP01, :YRDP02, :YRDP03, :YRDP04, :YRDP05, ' +
          ':YRDP06, :YRDP07, :YRDP08, :YRDP09, :YRDP10, :YRDP11, :YRDP12, :YRDP13, ' +
          ':YRDP14, :YRDP15, :YRDP16)');
      OpenQuery(AdoQuery,
          'select yrco#, yrempn, yrmgrn, yrjobd, yrjobl,  ' +
          '  case ' +
          '    when p.yrhdte = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrhdte),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrhdte)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrhdte),4,2)||''-''|| ''0'' || left(trim(p.yrhdte),1) || ''-'' ||substr(trim(p.yrhdte),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrhdte),5,2)||''-''|| left(trim(p.yrhdte),2) || ''-'' ||substr(trim(p.yrhdte),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrhdte),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrhdte)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrhdte),4,2)||''-''|| ''0'' || left(trim(p.yrhdte),1) || ''-'' ||substr(trim(p.yrhdte),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrhdte),5,2)||''-''|| left(trim(p.yrhdte),2) || ''-'' ||substr(trim(p.yrhdte),3,2) ' +
          '        end as date) ' +
          '  end as yrhdte, ' +
          '  case ' +
          '    when p.yrhdto = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrhdto),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrhdto)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrhdto),4,2)||''-''|| ''0'' || left(trim(p.yrhdto),1) || ''-'' ||substr(trim(p.yrhdto),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrhdto),5,2)||''-''|| left(trim(p.yrhdto),2) || ''-'' ||substr(trim(p.yrhdto),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrhdto),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrhdto)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrhdto),4,2)||''-''|| ''0'' || left(trim(p.yrhdto),1) || ''-'' ||substr(trim(p.yrhdto),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrhdto),5,2)||''-''|| left(trim(p.yrhdto),2) || ''-'' ||substr(trim(p.yrhdto),3,2) ' +
          '        end as date) ' +
          '  end as yrhdto, ' +
          '  case ' +
          '    when p.yrtdte = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrtdte),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrtdte)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrtdte),4,2)||''-''|| ''0'' || left(trim(p.yrtdte),1) || ''-'' ||substr(trim(p.yrtdte),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrtdte),5,2)||''-''|| left(trim(p.yrtdte),2) || ''-'' ||substr(trim(p.yrtdte),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrtdte),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrtdte)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrtdte),4,2)||''-''|| ''0'' || left(trim(p.yrtdte),1) || ''-'' ||substr(trim(p.yrtdte),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrtdte),5,2)||''-''|| left(trim(p.yrtdte),2) || ''-'' ||substr(trim(p.yrtdte),3,2) ' +
          '        end as date) ' +
          '  end as yrtdte, ' +
          '  case ' +
          '    when p.yrrdte = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrrdte),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrrdte)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrrdte),4,2)||''-''|| ''0'' || left(trim(p.yrrdte),1) || ''-'' ||substr(trim(p.yrrdte),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrrdte),5,2)||''-''|| left(trim(p.yrrdte),2) || ''-'' ||substr(trim(p.yrrdte),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrrdte),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrrdte)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrrdte),4,2)||''-''|| ''0'' || left(trim(p.yrrdte),1) || ''-'' ||substr(trim(p.yrrdte),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrrdte),5,2)||''-''|| left(trim(p.yrrdte),2) || ''-'' ||substr(trim(p.yrrdte),3,2) ' +
          '        end as date) ' +
          '  end as yrrdte,  ' +
          '  case ' +
          '    when p.yrndte = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrndte),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrndte)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrndte),4,2)||''-''|| ''0'' || left(trim(p.yrndte),1) || ''-'' ||substr(trim(p.yrndte),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrndte),5,2)||''-''|| left(trim(p.yrndte),2) || ''-'' ||substr(trim(p.yrndte),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrndte),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrndte)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrndte),4,2)||''-''|| ''0'' || left(trim(p.yrndte),1) || ''-'' ||substr(trim(p.yrndte),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrndte),5,2)||''-''|| left(trim(p.yrndte),2) || ''-'' ||substr(trim(p.yrndte),3,2) ' +
          '        end as date) ' +
          '  end as yrndte,  ' +
          '  case ' +
          '    when p.yrcdte = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrcdte),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrcdte)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrcdte),4,2)||''-''|| ''0'' || left(trim(p.yrcdte),1) || ''-'' ||substr(trim(p.yrcdte),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrcdte),5,2)||''-''|| left(trim(p.yrcdte),2) || ''-'' ||substr(trim(p.yrcdte),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrcdte),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrcdte)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrcdte),4,2)||''-''|| ''0'' || left(trim(p.yrcdte),1) || ''-'' ||substr(trim(p.yrcdte),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrcdte),5,2)||''-''|| left(trim(p.yrcdte),2) || ''-'' ||substr(trim(p.yrcdte),3,2) ' +
          '        end as date) ' +
          '  end as yrcdte,     ' +
          '  yrbp01, yrbp02, yrbp03, yrbp04, yrbp05, yrbp06, yrbp07, yrbp08, ' +
          '  yrbp09, yrbp10, yrbp11, yrbp12, yrbp13, yrbp14, yrbp15, yrbp16, ' +
          '  case ' +
          '    when p.yrdp01 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp01),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp01)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp01),4,2)||''-''|| ''0'' || left(trim(p.yrdp01),1) || ''-'' ||substr(trim(p.yrdp01),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp01),5,2)||''-''|| left(trim(p.yrdp01),2) || ''-'' ||substr(trim(p.yrdp01),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp01),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp01)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp01),4,2)||''-''|| ''0'' || left(trim(p.yrdp01),1) || ''-'' ||substr(trim(p.yrdp01),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp01),5,2)||''-''|| left(trim(p.yrdp01),2) || ''-'' ||substr(trim(p.yrdp01),3,2) ' +
          '        end as date) ' +
          '  end as yrdp01,   ' +
          '  case ' +
          '    when p.yrdp02 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp02),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp02)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp02),4,2)||''-''|| ''0'' || left(trim(p.yrdp02),1) || ''-'' ||substr(trim(p.yrdp02),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp02),5,2)||''-''|| left(trim(p.yrdp02),2) || ''-'' ||substr(trim(p.yrdp02),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp02),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp02)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp02),4,2)||''-''|| ''0'' || left(trim(p.yrdp02),1) || ''-'' ||substr(trim(p.yrdp02),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp02),5,2)||''-''|| left(trim(p.yrdp02),2) || ''-'' ||substr(trim(p.yrdp02),3,2) ' +
          '        end as date) ' +
          '  end as yrdp02,   ' +
          '  case ' +
          '    when p.yrdp03 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp03),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp03)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp03),4,2)||''-''|| ''0'' || left(trim(p.yrdp03),1) || ''-'' ||substr(trim(p.yrdp03),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp03),5,2)||''-''|| left(trim(p.yrdp03),2) || ''-'' ||substr(trim(p.yrdp03),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp03),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp03)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp03),4,2)||''-''|| ''0'' || left(trim(p.yrdp03),1) || ''-'' ||substr(trim(p.yrdp03),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp03),5,2)||''-''|| left(trim(p.yrdp03),2) || ''-'' ||substr(trim(p.yrdp03),3,2) ' +
          '        end as date) ' +
          '  end as yrdp03,   ' +
          '  case ' +
          '    when p.yrdp04 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp04),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp04)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp04),4,2)||''-''|| ''0'' || left(trim(p.yrdp04),1) || ''-'' ||substr(trim(p.yrdp04),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp04),5,2)||''-''|| left(trim(p.yrdp04),2) || ''-'' ||substr(trim(p.yrdp04),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp04),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp04)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp04),4,2)||''-''|| ''0'' || left(trim(p.yrdp04),1) || ''-'' ||substr(trim(p.yrdp04),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp04),5,2)||''-''|| left(trim(p.yrdp04),2) || ''-'' ||substr(trim(p.yrdp04),3,2) ' +
          '        end as date) ' +
          '  end as yrdp04,   ' +
          '  case ' +
          '    when p.yrdp05 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp05),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp05)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp05),4,2)||''-''|| ''0'' || left(trim(p.yrdp05),1) || ''-'' ||substr(trim(p.yrdp05),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp05),5,2)||''-''|| left(trim(p.yrdp05),2) || ''-'' ||substr(trim(p.yrdp05),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp05),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp05)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp05),4,2)||''-''|| ''0'' || left(trim(p.yrdp05),1) || ''-'' ||substr(trim(p.yrdp05),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp05),5,2)||''-''|| left(trim(p.yrdp05),2) || ''-'' ||substr(trim(p.yrdp05),3,2) ' +
          '        end as date) ' +
          '  end as yrdp05,   ' +
          '  case ' +
          '    when p.yrdp06 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp06),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp06)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp06),4,2)||''-''|| ''0'' || left(trim(p.yrdp06),1) || ''-'' ||substr(trim(p.yrdp06),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp06),5,2)||''-''|| left(trim(p.yrdp06),2) || ''-'' ||substr(trim(p.yrdp06),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp06),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp06)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp06),4,2)||''-''|| ''0'' || left(trim(p.yrdp06),1) || ''-'' ||substr(trim(p.yrdp06),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp06),5,2)||''-''|| left(trim(p.yrdp06),2) || ''-'' ||substr(trim(p.yrdp06),3,2) ' +
          '        end as date) ' +
          '  end as yrdp06,   ' +
          '  case ' +
          '    when p.yrdp07 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp07),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp07)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp07),4,2)||''-''|| ''0'' || left(trim(p.yrdp07),1) || ''-'' ||substr(trim(p.yrdp07),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp07),5,2)||''-''|| left(trim(p.yrdp07),2) || ''-'' ||substr(trim(p.yrdp07),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp07),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp07)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp07),4,2)||''-''|| ''0'' || left(trim(p.yrdp07),1) || ''-'' ||substr(trim(p.yrdp07),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp07),5,2)||''-''|| left(trim(p.yrdp07),2) || ''-'' ||substr(trim(p.yrdp07),3,2) ' +
          '        end as date) ' +
          '  end as yrdp07,   ' +
          '  case ' +
          '    when p.yrdp08 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp08),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp08)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp08),4,2)||''-''|| ''0'' || left(trim(p.yrdp08),1) || ''-'' ||substr(trim(p.yrdp08),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp08),5,2)||''-''|| left(trim(p.yrdp08),2) || ''-'' ||substr(trim(p.yrdp08),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp08),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp08)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp08),4,2)||''-''|| ''0'' || left(trim(p.yrdp08),1) || ''-'' ||substr(trim(p.yrdp08),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp08),5,2)||''-''|| left(trim(p.yrdp08),2) || ''-'' ||substr(trim(p.yrdp08),3,2) ' +
          '        end as date) ' +
          '  end as yrdp08,    ' +
          '  case ' +
          '    when p.yrdp09 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp09),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp09)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp09),4,2)||''-''|| ''0'' || left(trim(p.yrdp09),1) || ''-'' ||substr(trim(p.yrdp09),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp09),5,2)||''-''|| left(trim(p.yrdp09),2) || ''-'' ||substr(trim(p.yrdp09),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp09),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp09)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp09),4,2)||''-''|| ''0'' || left(trim(p.yrdp09),1) || ''-'' ||substr(trim(p.yrdp09),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp09),5,2)||''-''|| left(trim(p.yrdp09),2) || ''-'' ||substr(trim(p.yrdp09),3,2) ' +
          '        end as date) ' +
          '  end as yrdp09,    ' +
          '  case ' +
          '    when p.yrdp10 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp10),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp10)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp10),4,2)||''-''|| ''0'' || left(trim(p.yrdp10),1) || ''-'' ||substr(trim(p.yrdp10),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp10),5,2)||''-''|| left(trim(p.yrdp10),2) || ''-'' ||substr(trim(p.yrdp10),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp10),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp10)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp10),4,2)||''-''|| ''0'' || left(trim(p.yrdp10),1) || ''-'' ||substr(trim(p.yrdp10),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp10),5,2)||''-''|| left(trim(p.yrdp10),2) || ''-'' ||substr(trim(p.yrdp10),3,2) ' +
          '        end as date) ' +
          '  end as yrdp10,      ' +
          '  case ' +
          '    when p.yrdp11 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp11),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp11)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp11),4,2)||''-''|| ''0'' || left(trim(p.yrdp11),1) || ''-'' ||substr(trim(p.yrdp11),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp11),5,2)||''-''|| left(trim(p.yrdp11),2) || ''-'' ||substr(trim(p.yrdp11),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp11),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp11)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp11),4,2)||''-''|| ''0'' || left(trim(p.yrdp11),1) || ''-'' ||substr(trim(p.yrdp11),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp11),5,2)||''-''|| left(trim(p.yrdp11),2) || ''-'' ||substr(trim(p.yrdp11),3,2) ' +
          '        end as date) ' +
          '  end as yrdp11,  ' +
          '  case ' +
          '    when p.yrdp12 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp12),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp12)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp12),4,2)||''-''|| ''0'' || left(trim(p.yrdp12),1) || ''-'' ||substr(trim(p.yrdp12),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp12),5,2)||''-''|| left(trim(p.yrdp12),2) || ''-'' ||substr(trim(p.yrdp12),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp12),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp12)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp12),4,2)||''-''|| ''0'' || left(trim(p.yrdp12),1) || ''-'' ||substr(trim(p.yrdp12),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp12),5,2)||''-''|| left(trim(p.yrdp12),2) || ''-'' ||substr(trim(p.yrdp12),3,2) ' +
          '        end as date) ' +
          '  end as yrdp12,     ' +
          '  case ' +
          '    when p.yrdp13 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp13),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp13)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp13),4,2)||''-''|| ''0'' || left(trim(p.yrdp13),1) || ''-'' ||substr(trim(p.yrdp13),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp13),5,2)||''-''|| left(trim(p.yrdp13),2) || ''-'' ||substr(trim(p.yrdp13),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp13),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp13)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp13),4,2)||''-''|| ''0'' || left(trim(p.yrdp13),1) || ''-'' ||substr(trim(p.yrdp13),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp13),5,2)||''-''|| left(trim(p.yrdp13),2) || ''-'' ||substr(trim(p.yrdp13),3,2) ' +
          '        end as date) ' +
          '  end as yrdp13,   ' +
          '  case ' +
          '    when p.yrdp14 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp14),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp14)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp14),4,2)||''-''|| ''0'' || left(trim(p.yrdp14),1) || ''-'' ||substr(trim(p.yrdp14),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp14),5,2)||''-''|| left(trim(p.yrdp14),2) || ''-'' ||substr(trim(p.yrdp14),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp14),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp14)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp14),4,2)||''-''|| ''0'' || left(trim(p.yrdp14),1) || ''-'' ||substr(trim(p.yrdp14),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp14),5,2)||''-''|| left(trim(p.yrdp14),2) || ''-'' ||substr(trim(p.yrdp14),3,2) ' +
          '        end as date) ' +
          '  end as yrdp14,   ' +
          '  case ' +
          '    when p.yrdp15 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp15),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp15)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp15),4,2)||''-''|| ''0'' || left(trim(p.yrdp15),1) || ''-'' ||substr(trim(p.yrdp15),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp15),5,2)||''-''|| left(trim(p.yrdp15),2) || ''-'' ||substr(trim(p.yrdp15),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp15),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp15)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp15),4,2)||''-''|| ''0'' || left(trim(p.yrdp15),1) || ''-'' ||substr(trim(p.yrdp15),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp15),5,2)||''-''|| left(trim(p.yrdp15),2) || ''-'' ||substr(trim(p.yrdp15),3,2) ' +
          '        end as date) ' +
          '  end as yrdp15,  ' +
          '  case ' +
          '    when p.yrdp16 = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.yrdp16),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp16)) ' +
          '          when 5 then  ''20''||substr(trim(p.yrdp16),4,2)||''-''|| ''0'' || left(trim(p.yrdp16),1) || ''-'' ||substr(trim(p.yrdp16),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.yrdp16),5,2)||''-''|| left(trim(p.yrdp16),2) || ''-'' ||substr(trim(p.yrdp16),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.yrdp16),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.yrdp16)) ' +
          '          when 5 then  ''19''||substr(trim(p.yrdp16),4,2)||''-''|| ''0'' || left(trim(p.yrdp16),1) || ''-'' ||substr(trim(p.yrdp16),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.yrdp16),5,2)||''-''|| left(trim(p.yrdp16),2) || ''-'' ||substr(trim(p.yrdp16),3,2) ' +
          '        end as date) ' +
          '  end as yrdp16 ' +
          'from rydedata.pyprhead p');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('yrco').AsString := AdoQuery.FieldByName('yrco#').AsString;
        AdsQuery.ParamByName('yrempn').AsString := AdoQuery.FieldByName('yrempn').AsString;
        AdsQuery.ParamByName('yrmgrn').AsString := AdoQuery.FieldByName('yrmgrn').AsString;
        AdsQuery.ParamByName('yrjobd').AsString := AdoQuery.FieldByName('yrjobd').AsString;
        AdsQuery.ParamByName('yrjobl').AsString := AdoQuery.FieldByName('yrjobl').AsString;
        AdsQuery.ParamByName('yrhdte').AsDateTime := AdoQuery.FieldByName('yrhdte').AsDateTime;
        AdsQuery.ParamByName('yrhdto').AsDateTime := AdoQuery.FieldByName('yrhdto').AsDateTime;
        AdsQuery.ParamByName('yrtdte').AsDateTime := AdoQuery.FieldByName('yrtdte').AsDateTime;
        AdsQuery.ParamByName('yrrdte').AsDateTime := AdoQuery.FieldByName('yrrdte').AsDateTime;
        AdsQuery.ParamByName('yrndte').AsDateTime := AdoQuery.FieldByName('yrndte').AsDateTime;
        AdsQuery.ParamByName('yrcdte').AsDateTime := AdoQuery.FieldByName('yrcdte').AsDateTime;
        AdsQuery.ParamByName('yrbp01').AsCurrency := AdoQuery.FieldByName('yrbp01').AsCurrency;
        AdsQuery.ParamByName('yrbp02').AsCurrency := AdoQuery.FieldByName('yrbp02').AsCurrency;
        AdsQuery.ParamByName('yrbp03').AsCurrency := AdoQuery.FieldByName('yrbp03').AsCurrency;
        AdsQuery.ParamByName('yrbp04').AsCurrency := AdoQuery.FieldByName('yrbp04').AsCurrency;
        AdsQuery.ParamByName('yrbp05').AsCurrency := AdoQuery.FieldByName('yrbp05').AsCurrency;
        AdsQuery.ParamByName('yrbp06').AsCurrency := AdoQuery.FieldByName('yrbp06').AsCurrency;
        AdsQuery.ParamByName('yrbp07').AsCurrency := AdoQuery.FieldByName('yrbp07').AsCurrency;
        AdsQuery.ParamByName('yrbp08').AsCurrency := AdoQuery.FieldByName('yrbp08').AsCurrency;
        AdsQuery.ParamByName('yrbp09').AsCurrency := AdoQuery.FieldByName('yrbp09').AsCurrency;
        AdsQuery.ParamByName('yrbp10').AsCurrency := AdoQuery.FieldByName('yrbp10').AsCurrency;
        AdsQuery.ParamByName('yrbp11').AsCurrency := AdoQuery.FieldByName('yrbp11').AsCurrency;
        AdsQuery.ParamByName('yrbp12').AsCurrency := AdoQuery.FieldByName('yrbp12').AsCurrency;
        AdsQuery.ParamByName('yrbp13').AsCurrency := AdoQuery.FieldByName('yrbp13').AsCurrency;
        AdsQuery.ParamByName('yrbp14').AsCurrency := AdoQuery.FieldByName('yrbp14').AsCurrency;
        AdsQuery.ParamByName('yrbp15').AsCurrency := AdoQuery.FieldByName('yrbp15').AsCurrency;
        AdsQuery.ParamByName('yrbp16').AsCurrency := AdoQuery.FieldByName('yrbp16').AsCurrency;
        AdsQuery.ParamByName('yrdp01').AsDateTime := AdoQuery.FieldByName('yrdp01').AsDateTime;
        AdsQuery.ParamByName('yrdp02').AsDateTime := AdoQuery.FieldByName('yrdp02').AsDateTime;
        AdsQuery.ParamByName('yrdp03').AsDateTime := AdoQuery.FieldByName('yrdp03').AsDateTime;
        AdsQuery.ParamByName('yrdp04').AsDateTime := AdoQuery.FieldByName('yrdp04').AsDateTime;
        AdsQuery.ParamByName('yrdp05').AsDateTime := AdoQuery.FieldByName('yrdp05').AsDateTime;
        AdsQuery.ParamByName('yrdp06').AsDateTime := AdoQuery.FieldByName('yrdp06').AsDateTime;
        AdsQuery.ParamByName('yrdp07').AsDateTime := AdoQuery.FieldByName('yrdp07').AsDateTime;
        AdsQuery.ParamByName('yrdp08').AsDateTime := AdoQuery.FieldByName('yrdp08').AsDateTime;
        AdsQuery.ParamByName('yrdp09').AsDateTime := AdoQuery.FieldByName('yrdp09').AsDateTime;
        AdsQuery.ParamByName('yrdp10').AsDateTime := AdoQuery.FieldByName('yrdp10').AsDateTime;
        AdsQuery.ParamByName('yrdp11').AsDateTime := AdoQuery.FieldByName('yrdp11').AsDateTime;
        AdsQuery.ParamByName('yrdp12').AsDateTime := AdoQuery.FieldByName('yrdp12').AsDateTime;
        AdsQuery.ParamByName('yrdp13').AsDateTime := AdoQuery.FieldByName('yrdp13').AsDateTime;
        AdsQuery.ParamByName('yrdp14').AsDateTime := AdoQuery.FieldByName('yrdp14').AsDateTime;
        AdsQuery.ParamByName('yrdp15').AsDateTime := AdoQuery.FieldByName('yrdp15').AsDateTime;
        AdsQuery.ParamByName('yrdp16').AsDateTime := AdoQuery.FieldByName('yrdp16').AsDateTime;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYPRHEAD'')');
      ScrapeCountTable('PYPRHEAD', 'stgArkonaPYPRHEAD');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPRHEAD.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.PYACTGR;
var
  proc: string;
begin
  proc := 'PYACTGR';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpPYACTGR'')');
//      ExecuteQuery(AdsQuery, 'insert into tmpPYACTGR select * from stgArkonaPYACTGR');
//      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYACTGR'')');
//      PrepareQuery(AdsQuery, ' insert into stgArkonaPYACTGR(' +
      PrepareQuery(AdsQuery, ' insert into tmpPYACTGR(' +
          'COMPANY_NUMBER, DIST_CODE, SEQ_NUMBER, DESCRIPTION, DIST_CO_, ' +
          'GROSS_DIST, GROSS_EXPENSE_ACT_, OVERTIME_ACT_, VACATION_EXPENSE_ACT_, ' +
          'HOLIDAY_EXPENSE_ACT_, SICK_LEAVE_EXPENSE_ACT_, RETIRE_EXPENSE_ACT_, ' +
          'EMPLR_FICA_EXPENSE, EMPLR_MED_EXPENSE, FEDERAL_UN_EMP_ACT_, ' +
          'STATE_UNEMP_ACT_, STATE_COMP_ACT_, STATE_SDI_ACT_, EMPLR_CONTRIBUTIONS) ' +
          'values(' +
          ':COMPANY_NUMBER, :DIST_CODE, :SEQ_NUMBER, :DESCRIPTION, :DIST_CO_, ' +
          ':GROSS_DIST, :GROSS_EXPENSE_ACT_, :OVERTIME_ACT_, :VACATION_EXPENSE_ACT_, ' +
          ':HOLIDAY_EXPENSE_ACT_, :SICK_LEAVE_EXPENSE_ACT_, :RETIRE_EXPENSE_ACT_, ' +
          ':EMPLR_FICA_EXPENSE, :EMPLR_MED_EXPENSE, :FEDERAL_UN_EMP_ACT_, ' +
          ':STATE_UNEMP_ACT_, :STATE_COMP_ACT_, :STATE_SDI_ACT_, :EMPLR_CONTRIBUTIONS)');
      OpenQuery(AdoQuery, 'select * from rydedata.pyactgr');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('COMPANY_NUMBER').AsString := AdoQuery.FieldByName('COMPANY_NUMBER').AsString;
        AdsQuery.ParamByName('DIST_CODE').AsString := AdoQuery.FieldByName('DIST_CODE').AsString;
        AdsQuery.ParamByName('SEQ_NUMBER').AsInteger := AdoQuery.FieldByName('SEQ_NUMBER').AsInteger;
        AdsQuery.ParamByName('DESCRIPTION').AsString := AdoQuery.FieldByName('DESCRIPTION').AsString;
        AdsQuery.ParamByName('DIST_CO_').AsString := AdoQuery.FieldByName('DIST_CO_').AsString;
        AdsQuery.ParamByName('GROSS_DIST').AsFloat := AdoQuery.FieldByName('GROSS_DIST').AsFloat;
        AdsQuery.ParamByName('GROSS_EXPENSE_ACT_').AsString := AdoQuery.FieldByName('GROSS_EXPENSE_ACT_').AsString;
        AdsQuery.ParamByName('OVERTIME_ACT_').AsString := AdoQuery.FieldByName('OVERTIME_ACT_').AsString;
        AdsQuery.ParamByName('VACATION_EXPENSE_ACT_').AsString := AdoQuery.FieldByName('VACATION_EXPENSE_ACT_').AsString;
        AdsQuery.ParamByName('HOLIDAY_EXPENSE_ACT_').AsString := AdoQuery.FieldByName('HOLIDAY_EXPENSE_ACT_').AsString;
        AdsQuery.ParamByName('SICK_LEAVE_EXPENSE_ACT_').AsString := AdoQuery.FieldByName('SICK_LEAVE_EXPENSE_ACT_').AsString;
        AdsQuery.ParamByName('RETIRE_EXPENSE_ACT_').AsString := AdoQuery.FieldByName('RETIRE_EXPENSE_ACT_').AsString;
        AdsQuery.ParamByName('EMPLR_FICA_EXPENSE').AsString := AdoQuery.FieldByName('EMPLR_FICA_EXPENSE').AsString;
        AdsQuery.ParamByName('EMPLR_MED_EXPENSE').AsString := AdoQuery.FieldByName('EMPLR_MED_EXPENSE').AsString;
        AdsQuery.ParamByName('FEDERAL_UN_EMP_ACT_').AsString := AdoQuery.FieldByName('FEDERAL_UN_EMP_ACT_').AsString;
        AdsQuery.ParamByName('STATE_UNEMP_ACT_').AsString := AdoQuery.FieldByName('STATE_UNEMP_ACT_').AsString;
        AdsQuery.ParamByName('STATE_COMP_ACT_').AsString := AdoQuery.FieldByName('STATE_COMP_ACT_').AsString;
        AdsQuery.ParamByName('STATE_SDI_ACT_').AsString := AdoQuery.FieldByName('STATE_SDI_ACT_').AsString;
        AdsQuery.ParamByName('EMPLR_CONTRIBUTIONS').AsString := AdoQuery.FieldByName('EMPLR_CONTRIBUTIONS').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPYACTGR'')');
      ScrapeCountTable('pyactgr', 'tmpPYACTGR');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPYACTGR()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPACTGR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDM.PYMAST;
var
  proc: string;
begin
  proc := 'PYMAST';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYMAST'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaPYMAST (' +
          'YMCO#, YMEMPN, YMACTV, YMDEPT, YMSTCD, YMNAME, YMADD1, YMCITY, YMSTAT, ' +
          'YMZIP, YMAREA, YMPHON, YMSS#, YMDRIV, YMBDTE, YMHDTE, YMHDTO, YMRDTE, ' +
          'YMTDTE, YMSEX, YMMARI, YMFDEX, YMFDAD, YMFDAP, YMCLAS, YMEEIC, YMECLS, ' +
          'YMPPER, YMSALY, YMRATE, YMDIST, YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, ' +
          'YMAR#, YMDVST, YMFDST, YMSTST) ' +
          'values (' +
          ':YMCO, :YMEMPN, :YMACTV, :YMDEPT, :YMSTCD, :YMNAME, :YMADD1, :YMCITY, :YMSTAT, ' +
          ':YMZIP, :YMAREA, :YMPHON, :YMSS, :YMDRIV, :YMBDTE, :YMHDTE, :YMHDTO, :YMRDTE, ' +
          ':YMTDTE, :YMSEX, :YMMARI, :YMFDEX, :YMFDAD, :YMFDAP, :YMCLAS, :YMEEIC, :YMECLS, ' +
          ':YMPPER, :YMSALY, :YMRATE, :YMDIST, :YMRETE, :YMSTTX, :YMCNTX, :YMSTEX, :YMSBKI, ' +
          ':YMAR, :YMDVST, :YMFDST, :YMSTST)');
      OpenQuery(AdoQuery,
          'select YMCO#, YMEMPN, YMACTV, YMDEPT, YMSTCD, YMNAME, YMADD1, ' +
          '  YMCITY, YMSTAT, YMZIP, YMAREA, YMPHON, YMSS#, YMDRIV, ' +
          '  case ' +
          '    when cast(right(trim(p.YMBDTE),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMBDTE)) ' +
          '          when 5 then  ''20''||substr(trim(p.YMBDTE),4,2)||''-''|| ''0'' || left(trim(p.YMBDTE),1) || ''-'' ||substr(trim(p.YMBDTE),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.YMBDTE),5,2)||''-''|| left(trim(p.YMBDTE),2) || ''-'' ||substr(trim(p.YMBDTE),3,2) ' +
          '        end as date) ' +
          '    else ' +
          '      cast ( ' +
          '        case length(trim(p.YMBDTE)) ' +
          '          when 5 then  ''19''||substr(trim(p.YMBDTE),4,2)||''-''|| ''0'' || left(trim(p.YMBDTE),1) || ''-'' ||substr(trim(p.YMBDTE),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.YMBDTE),5,2)||''-''|| left(trim(p.YMBDTE),2) || ''-'' ||substr(trim(p.YMBDTE),3,2) ' +
          '        end as date) ' +
          '  end as YMBDTE, ' +
          '  case ' +
          '    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMHDTE)) ' +
          '          when 5 then  ''20''||substr(trim(p.YMHDTE),4,2)||''-''|| ''0'' || left(trim(p.YMHDTE),1) || ''-'' ||substr(trim(p.YMHDTE),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.YMHDTE),5,2)||''-''|| left(trim(p.YMHDTE),2) || ''-'' ||substr(trim(p.YMHDTE),3,2) ' +
          '        end as date) ' +
          '    else ' +
          '      cast ( ' +
          '        case length(trim(p.YMHDTE)) ' +
          '          when 5 then  ''19''||substr(trim(p.YMHDTE),4,2)||''-''|| ''0'' || left(trim(p.YMHDTE),1) || ''-'' ||substr(trim(p.YMHDTE),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.YMHDTE),5,2)||''-''|| left(trim(p.YMHDTE),2) || ''-'' ||substr(trim(p.YMHDTE),3,2) ' +
          '        end as date) ' +
          '  end as YMHDTE, ' +
          '  case ' +
          '    when p.YMHDTO = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.YMHDTO),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMHDTO)) ' +
          '          when 5 then  ''20''||substr(trim(p.YMHDTO),4,2)||''-''|| ''0'' || left(trim(p.YMHDTO),1) || ''-'' ||substr(trim(p.YMHDTO),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.YMHDTO),5,2)||''-''|| left(trim(p.YMHDTO),2) || ''-'' ||substr(trim(p.YMHDTO),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.YMHDTO),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMHDTO)) ' +
          '          when 5 then  ''19''||substr(trim(p.YMHDTO),4,2)||''-''|| ''0'' || left(trim(p.YMHDTO),1) || ''-'' ||substr(trim(p.YMHDTO),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.YMHDTO),5,2)||''-''|| left(trim(p.YMHDTO),2) || ''-'' ||substr(trim(p.YMHDTO),3,2) ' +
          '        end as date) ' +
          '  end as YMHDTO, ' +
          '  case ' +
          '    when p.YMRDTE = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.YMRDTE),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMRDTE)) ' +
          '          when 5 then  ''20''||substr(trim(p.YMRDTE),4,2)||''-''|| ''0'' || left(trim(p.YMRDTE),1) || ''-'' ||substr(trim(p.YMRDTE),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.YMRDTE),5,2)||''-''|| left(trim(p.YMRDTE),2) || ''-'' ||substr(trim(p.YMRDTE),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.YMRDTE),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMRDTE)) ' +
          '          when 5 then  ''19''||substr(trim(p.YMRDTE),4,2)||''-''|| ''0'' || left(trim(p.YMRDTE),1) || ''-'' ||substr(trim(p.YMRDTE),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.YMRDTE),5,2)||''-''|| left(trim(p.YMRDTE),2) || ''-'' ||substr(trim(p.YMRDTE),3,2) ' +
          '        end as date) ' +
          '  end as YMRDTE, ' +
          '  case ' +
          '    when p.YMTDTE = 0 then date(''9999-12-31'') ' +
          '    when cast(right(trim(p.YMTDTE),2) as integer) < 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMTDTE)) ' +
          '          when 5 then  ''20''||substr(trim(p.YMTDTE),4,2)||''-''|| ''0'' || left(trim(p.YMTDTE),1) || ''-'' ||substr(trim(p.YMTDTE),2,2) ' +
          '          when 6 then  ''20''||substr(trim(p.YMTDTE),5,2)||''-''|| left(trim(p.YMTDTE),2) || ''-'' ||substr(trim(p.YMTDTE),3,2) ' +
          '        end as date) ' +
          '    when cast(right(trim(p.YMTDTE),2) as integer) >= 20 then ' +
          '      cast ( ' +
          '        case length(trim(p.YMTDTE)) ' +
          '          when 5 then  ''19''||substr(trim(p.YMTDTE),4,2)||''-''|| ''0'' || left(trim(p.YMTDTE),1) || ''-'' ||substr(trim(p.YMTDTE),2,2) ' +
          '          when 6 then  ''19''||substr(trim(p.YMTDTE),5,2)||''-''|| left(trim(p.YMTDTE),2) || ''-'' ||substr(trim(p.YMTDTE),3,2) ' +
          '        end as date) ' +
          '  end as YMTDTE, ' +
          '  YMSEX, YMMARI, YMFDEX, YMFDAD, ' +
          '  YMFDAP, YMCLAS, YMEEIC, YMECLS, YMPPER, YMSALY, YMRATE, YMDIST, ' +
          '  YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, YMAR#, YMDVST, YMFDST, YMSTST ' +
          'from rydedata.pymast p ' +
          'where length(trim(ymname)) > 3 ' +
          '  and trim(ymname) <> ''TEST'' ');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ymco').AsString := AdoQuery.FieldByName('ymco#').AsString;
        AdsQuery.ParamByName('ymempn').AsString := AdoQuery.FieldByName('ymempn').AsString;
        AdsQuery.ParamByName('ymactv').AsString := AdoQuery.FieldByName('ymactv').AsString;
        AdsQuery.ParamByName('ymdept').AsString := AdoQuery.FieldByName('ymdept').AsString;
        AdsQuery.ParamByName('ymstcd').AsString := AdoQuery.FieldByName('ymstcd').AsString;
        AdsQuery.ParamByName('ymname').AsString := AdoQuery.FieldByName('ymname').AsString;
        AdsQuery.ParamByName('ymadd1').AsString := AdoQuery.FieldByName('ymadd1').AsString;
        AdsQuery.ParamByName('ymcity').AsString := AdoQuery.FieldByName('ymcity').AsString;
        AdsQuery.ParamByName('ymstat').AsString := AdoQuery.FieldByName('ymstat').AsString;
        if AdoQuery.FieldByName('ymzip').AsString = '0' then
          AdsQuery.ParamByName('ymzip').Clear
        else
          AdsQuery.ParamByName('ymzip').AsString := AdoQuery.FieldByName('ymzip').AsString;
        if AdoQuery.FieldByName('ymarea').AsString = '0' then
          AdsQuery.ParamByName('ymarea').Clear
        else
          AdsQuery.ParamByName('ymarea').AsString := AdoQuery.FieldByName('ymarea').AsString;
        if AdoQuery.FieldByName('ymphon').AsString = '0' then
          AdsQuery.ParamByName('ymphon').Clear
        else
          AdsQuery.ParamByName('ymphon').AsString := AdoQuery.FieldByName('ymphon').AsString;
        if AdoQuery.FieldByName('ymss#').AsString = '0' then
          AdsQuery.ParamByName('ymss').Clear
        else
          AdsQuery.ParamByName('ymss').AsString := AdoQuery.FieldByName('ymss#').AsString;
        AdsQuery.ParamByName('ymdriv').AsString := AdoQuery.FieldByName('ymdriv').AsString;
        AdsQuery.ParamByName('ymbdte').AsDateTime := AdoQuery.FieldByName('ymbdte').AsDateTime;
        AdsQuery.ParamByName('ymhdte').AsDateTime := AdoQuery.FieldByName('ymhdte').AsDateTime;
        AdsQuery.ParamByName('ymhdto').AsDateTime := AdoQuery.FieldByName('ymhdto').AsDateTime;
        AdsQuery.ParamByName('ymrdte').AsDateTime := AdoQuery.FieldByName('ymrdte').AsDateTime;
        AdsQuery.ParamByName('ymtdte').AsDateTime := AdoQuery.FieldByName('ymtdte').AsDateTime;
        AdsQuery.ParamByName('ymsex').AsString := AdoQuery.FieldByName('ymsex').AsString;
        AdsQuery.ParamByName('ymmari').AsString := AdoQuery.FieldByName('ymmari').AsString;
        AdsQuery.ParamByName('ymfdex').AsInteger := AdoQuery.FieldByName('ymfdex').AsInteger;
        AdsQuery.ParamByName('ymfdad').AsCurrency := AdoQuery.FieldByName('ymfdad').AsCurrency;
        AdsQuery.ParamByName('ymfdap').AsFloat := AdoQuery.FieldByName('ymfdap').AsFloat;
        AdsQuery.ParamByName('ymclas').AsString := AdoQuery.FieldByName('ymclas').AsString;
        AdsQuery.ParamByName('ymeeic').AsString := AdoQuery.FieldByName('ymeeic').AsString;
        AdsQuery.ParamByName('ymecls').AsString := AdoQuery.FieldByName('ymecls').AsString;
        AdsQuery.ParamByName('ympper').AsString := AdoQuery.FieldByName('ympper').AsString;
        AdsQuery.ParamByName('ymsaly').AsCurrency := AdoQuery.FieldByName('ymsaly').AsCurrency;
        AdsQuery.ParamByName('ymrate').AsCurrency := AdoQuery.FieldByName('ymrate').AsCurrency;
        AdsQuery.ParamByName('ymdist').AsString := AdoQuery.FieldByName('ymdist').AsString;
        AdsQuery.ParamByName('ymrete').AsString := AdoQuery.FieldByName('ymrete').AsString;
        AdsQuery.ParamByName('ymsttx').AsString := AdoQuery.FieldByName('ymsttx').AsString;
        AdsQuery.ParamByName('ymcntx').AsString := AdoQuery.FieldByName('ymcntx').AsString;
        AdsQuery.ParamByName('ymstex').AsInteger := AdoQuery.FieldByName('ymstex').AsInteger;
        AdsQuery.ParamByName('ymsbki').AsString := AdoQuery.FieldByName('ymsbki').AsString;
        AdsQuery.ParamByName('ymar').AsString := AdoQuery.FieldByName('ymar#').AsString;
        AdsQuery.ParamByName('ymdvst').AsString := AdoQuery.FieldByName('ymdvst').AsString;
        AdsQuery.ParamByName('ymfdst').AsString := AdoQuery.FieldByName('ymfdst').AsString;
        AdsQuery.ParamByName('ymstst').AsString := AdoQuery.FieldByName('ymstst').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      CloseQuery(AdsQuery);
      CloseQuery(AdoQuery);
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYMAST'')');
      ScrapeCountQuery(' rydedata.pymast p where length(trim(ymname)) > 3 ' +
          ' and trim(ymname) <> ''TEST'' ',
          ' stgArkonaPYMAST');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYMAST.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDM.xfmEmpDim;
var
  proc: string;
begin
  proc := 'xfmEmpDim';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure xfmEmpDim()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('xfmEmpDim.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;

procedure TDM.maintDimEmployee();
var
  proc: string;
  PassFail: string;
begin
  proc := 'maintDimEmployee';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      OpenQuery(AdsQuery, 'execute procedure maintDimEmployee()');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Fail' then
      begin
        sendmail('*** Maintenance Required ***', 'as indicated by EmpDim.maintDimEmployee');
      end;
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('fctClockHours.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;

//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;


procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;

procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;





procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
end.
