unit sdprdetDM;
(*)
6/28/15
out of the blue, this started throwing: SQL0666 - SQL query exceeds specified time limit or storage limit.

tried disabling the query timeout on the odbc config, did not help

// *a* fixed with this
http://stackoverflow.com/questions/5111467/sql0666-sql-query-exceeds-specified-time-limit-or-storage-limit

(**)
interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure tmpSDPRDET;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'SDPDRET';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\DDS\sunday\copy\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.tmpSDPRDET;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'tmpSDPRDET';
  dObject := 'tmpSDPRDET';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpSDPRDET');
          PrepareQuery(AdsQuery, 'insert into tmpSDPRDET values(' +
              ':PTCO, :PTRO, :PTLINE, :PTLTYP, :PTSEQ, :PTCODE, :PTDATE, :PTLPYM, ' +
              ':PTSVCTYP, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, :PTLHRS, :PTLAMT, ' +
              ':PTCOST, :PTARTF, :PTFAIL, :PTSLV, :PTSLI, :PTDCPN, :PTDBAS, ' +
              ':PTVATCODE, :PTVATAMT, :PTDISPTY1, :PTDISPTY2, :PTDISRANK1, ' +
              ':PTDISRANK2)');//, :PTPTAXAMT, :PTKTXAMT)');
// *a*
          adoquery.CommandTimeout := 0;
          OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, ' +
              ' case ' +
              '   when ptdate = 0 then cast(''9999-12-31'' as date) ' +
              ' else ' +
              '   cast(left(digits(ptdate), 4) || ''-'' || substr(digits(ptdate), 5, 2) || ''-'' || substr(digits(ptdate), 7, 2) as date) ' +
              ' end as PTDATE, ' +
              ' PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF, ' +
              ' PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2, ' +
              ' PTDISRANK1, PTDISRANK2 ' + //, PTPTAXAMT, PTKTXAMT ' +
              'from RYDEDATA.SDPRDET ' +
              'where trim(ptro#) in ( ' +
              '  select distinct trim(ptro#) ' +
              '  from rydedata.sdprhdr ' +
              '  where ptco# in (''RY1'',''RY2'') AND TRIM(PTRO#) <> '''' ' +
              '    AND ( ' +
              '      CAST(LEFT(DIGITS(PTDATE), 4) || ''-'' || SUBSTR(DIGITS(PTDATE), 5, 2) || ''-'' || SUBSTR(DIGITS(PTDATE), 7, 2) AS DATE) > CURDATE() - 45 days ' +
              '      OR (PTCDAT <> 0 AND ' +
              '        CAST(LEFT(DIGITS(PTCDAT), 4) || ''-'' || SUBSTR(DIGITS(PTCDAT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTCDAT), 7, 2) AS DATE) > CURDATE() - 45 days) ' +
              '      OR (PTFCDT <> 0 AND ' +
              '        CAST(LEFT(DIGITS(PTFCDT), 4) || ''-'' || SUBSTR(DIGITS(PTFCDT), 5, 2) || ''-'' || SUBSTR(DIGITS(PTFCDT), 7, 2) AS DATE) > CURDATE() - 45 days)) ' +
              '  union ' +
              '  select distinct trim(ptro#) ' +
              '  from rydedata.sdprdet ' +
              '  where ptco# in (''RY1'',''RY2'') AND TRIM(ptro#) <> '''' ' +
              '    and ptdate <> 0 ' +
              '    and CAST(LEFT(DIGITS(ptdate), 4) || ''-'' || SUBSTR(DIGITS(ptdate), 5, 2) || ''-'' || SUBSTR(DIGITS(ptdate), 7, 2) AS DATE) > CURDATE() - 45 days)');
//              ' where ptco# in (''RY1'', ''RY2'') ' +
//              '   and length(trim(ptro#)) > 6 ' +
//              '   and trim(ptro#) in (' +
//              '     select trim(ptro#) from rydedata.sdprhdr ' +
//              '     where ptco# in (''RY1'',''RY2'') ' +
//              '       and trim(ptro#) <> '''' ' +
//              '       and (  ' +
//              '         cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//              '         or ( ' +
//              '           ptcdat <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//              '         or (' +
//              '           ptfcdt <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))');
          while not AdoQuery.eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
            AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
            AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
            AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
            AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
            if  AdoQuery.FieldByName('ptdate').AsString = '12/31/9999' then
              AdsQuery.ParamByName('ptdate').Clear
            else
              AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
            AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
            AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
            AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
            AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
            AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
            AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
            AdsQuery.ParamByName('ptlamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
            AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
            AdsQuery.ParamByName('ptartf').AsString := AdoQuery.FieldByName('ptartf').AsString;
            AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
            AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
            AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
            AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
            AdsQuery.ParamByName('ptdbas').AsString := AdoQuery.FieldByName('ptdbas').AsString;
            AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
            AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
            AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
            AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
            AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
            AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end; //while not AdoQuery.eof do
//          ScrapeCountQuery(' rydedata.sdprdet ' +
//              ' where ptco# in (''RY1'', ''RY2'') ' +
//              '   and length(trim(ptro#)) > 6 ' +
//              '   and trim(ptro#) in (' +
//              '     select trim(ptro#) from rydedata.sdprhdr ' +
//              '     where ptco# in (''RY1'',''RY2'') ' +
//              '       and trim(ptro#) <> '''' ' +
//              '       and (  ' +
//              '         cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//              '         or ( ' +
//              '           ptcdat <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//              '         or (' +
//              '           ptfcdt <> 0 ' +
//              '           and  ' +
//              '           cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))',
//              ' tmpSDPRDET');
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpSDPRDET'')');
//          ExecuteQuery(AdsQuery, 'execute procedure stgArkSDPRDET()');
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
//
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


