unit pdpphdrDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure tmpPDPPHDR;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'PDPPHDR';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=Arkona Production';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.tmpPDPPHDR;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'tmpPDPPHDR';
  dObject := 'tmpPDPPHDR';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpPDPPHDR');
          PrepareQuery(AdsQuery, ' insert into tmpPDPPHDR(' +
              'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, PTDATE, PTDOC#, ' +
              'PTCUS#, PTCKEY, PTCNAM, PTPHON, PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
              'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
              'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
              'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
              'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
              'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
              'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
              'PTDTAR, PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
              'PTPRTY, PTRNT#, PTTIIN, PTCREATE, PTCTHOLD, PTAUTH#, PTAUTHID, ' +
              'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO) ' +
              'values(' +
              ':PTCO, :PTPKEY, :PTCPID, :PTSTAT, :PTDTYP, :PTTTYP, :PTDATE, :PTDOC, ' +
              ':PTCUS, :PTCKEY, :PTCNAM, :PTPHON, :PTSKEY, :PTPMTH, :PTSTYP, :PTPLVL, ' +
              ':PTTAXE, :PTPTOT, :PTSHPT, :PTSPOD, :PTSPDC, :PTSPDP, :PTSPDO, :PTSPOH, ' +
              ':PTSTAX, :PTSTAX2, :PTSTAX3, :PTSTAX4, :PTCPNM, :PTSRTK, :PTACTP, :PTCCAR, ' +
              ':PTPO, :PTREC, :PTODOC, :PTINTA, :PTRTYP, :PTRSTS, :PTWARO, :PTPAPRV, ' +
              ':PTSAPRV, :PTSWID, :PTRTCH, :PTTEST, :PTSVCO, :PTSVCO2, :PTDEDA, :PTDEDA2, ' +
              ':PTWDED, :PTVIN, :PTSTK, :PTTRCK, :PTFRAN, :PTODOM, :PTMILO, :PTRTIM, ' +
              ':PTTAG, :PTCHK, :PTHCMT, :PTSSOR, :PTHMOR, :PTLTOT, :PTSTOT, :PTDEDP, ' +
              ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
              ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, '  +
              ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
              ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTCPTD, :PTOSTS, ' +
              ':PTDTAR, :PTARCK, :PTWRO, :PTWSID, :PTAUTH, :PTAPDT, :PTDTLC, :PTDTPI, :PTIPTY, ' +
              ':PTPRTY, :PTRNT, :PTTIIN, :PTCREATE, :PTCTHOLD, :PTAUTH1, :PTAUTHID, ' +
              ':PTDLVMTH, :PTSHIPSEQ, :PTDSPTEAM, :PTTAXGO)');
          OpenQuery(AdoQuery, 'select ' +
              'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, ' +
              'cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) as PTDATE, ' +
              'PTDOC#, ' +
              'PTCUS#, PTCKEY, PTCNAM, ' +
              'CASE  WHEN PTPHON < 0 then 0 else ptphon end as ptphon, ' +
              'PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
              'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
              'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
              'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
              'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
              'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
              'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
              'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
              'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
              'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
              'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
              'case ' +
              '  when PTDTAR = 0 then cast(''9999-12-31'' as date) ' +
              '  else cast(left(digits(PTDTAR), 4) || ''-'' || substr(digits(PTDTAR), 5, 2) || ''-'' || substr(digits(PTDTAR), 7, 2) as date) ' +
              'end as PTDTAR, ' +
              'PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
              'PTPRTY, PTRNT#, PTTIIN, ' +
              'case ' +
              '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
              '  else PTCREATE ' +
              'end as PTCREATE, ' +
              'case ' +
              '  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
              '  else PTCTHOLD ' +
              'end as PTCTHOLD, ' +
              'PTAUTH#, PTAUTHID, ' +
              'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO ' +
              'from rydedata.pdpphdr where length(trim(PTDOC#)) > 6');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
            AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
            AdsQuery.ParamByName('ptstat').AsString := AdoQuery.FieldByName('ptstat').AsString;
            AdsQuery.ParamByName('ptdtyp').AsString := AdoQuery.FieldByName('ptdtyp').AsString;
            AdsQuery.ParamByName('ptttyp').AsString := AdoQuery.FieldByName('ptttyp').AsString;
            AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptdoc').AsString := AdoQuery.FieldByName('ptdoc#').AsString;
            AdsQuery.ParamByName('ptcus').AsString := AdoQuery.FieldByName('ptcus#').AsString;
            AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
            AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
            AdsQuery.ParamByName('ptphon').AsString := AdoQuery.FieldByName('ptphon').AsString;
            AdsQuery.ParamByName('ptskey').AsInteger := AdoQuery.FieldByName('ptskey').AsInteger;
            AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
            AdsQuery.ParamByName('ptstyp').AsString := AdoQuery.FieldByName('ptstyp').AsString;
            AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
            AdsQuery.ParamByName('pttaxe').AsString := AdoQuery.FieldByName('pttaxe').AsString;
            AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
            AdsQuery.ParamByName('ptshpt').AsCurrency := AdoQuery.FieldByName('ptshpt').AsCurrency;
            AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
            AdsQuery.ParamByName('ptspdc').AsCurrency := AdoQuery.FieldByName('ptspdc').AsCurrency;
            AdsQuery.ParamByName('ptspdp').AsCurrency := AdoQuery.FieldByName('ptspdp').AsCurrency;
            AdsQuery.ParamByName('ptspdo').AsString := AdoQuery.FieldByName('ptspdo').AsString;
            AdsQuery.ParamByName('ptspoh').AsString := AdoQuery.FieldByName('ptspoh').AsString;
            AdsQuery.ParamByName('ptstax').AsCurrency := AdoQuery.FieldByName('ptstax').AsCurrency;
            AdsQuery.ParamByName('ptstax2').AsCurrency := AdoQuery.FieldByName('ptstax2').AsCurrency;
            AdsQuery.ParamByName('ptstax3').AsCurrency := AdoQuery.FieldByName('ptstax3').AsCurrency;
            AdsQuery.ParamByName('ptstax4').AsCurrency := AdoQuery.FieldByName('ptstax4').AsCurrency;
            AdsQuery.ParamByName('ptcpnm').AsString := AdoQuery.FieldByName('ptcpnm').AsString;
            AdsQuery.ParamByName('ptsrtk').AsString := AdoQuery.FieldByName('ptsrtk').AsString;
            AdsQuery.ParamByName('ptactp').AsString := AdoQuery.FieldByName('ptactp').AsString;
            AdsQuery.ParamByName('ptccar').AsString := AdoQuery.FieldByName('ptccar').AsString;
            AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
            AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
            AdsQuery.ParamByName('ptodoc').AsString := AdoQuery.FieldByName('ptodoc').AsString;
            AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
            AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
            AdsQuery.ParamByName('ptrsts').AsString := AdoQuery.FieldByName('ptrsts').AsString;
            AdsQuery.ParamByName('ptwaro').AsString := AdoQuery.FieldByName('ptwaro').AsString;
            AdsQuery.ParamByName('ptpaprv').AsString := AdoQuery.FieldByName('ptpaprv').AsString;
            AdsQuery.ParamByName('ptsaprv').AsString := AdoQuery.FieldByName('ptsaprv').AsString;
            AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
            AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
            AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
            AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
            AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
            AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
            AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
            AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
            AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
            AdsQuery.ParamByName('ptstk').AsString := AdoQuery.FieldByName('ptstk#').AsString;
            AdsQuery.ParamByName('pttrck').AsString := AdoQuery.FieldByName('pttrck').AsString;
            AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
            AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
            AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
            AdsQuery.ParamByName('ptrtim').AsFloat := AdoQuery.FieldByName('ptrtim').AsFloat;
            AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
            AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
            AdsQuery.ParamByName('pthcmt').AsString := AdoQuery.FieldByName('pthcmt').AsString;
            AdsQuery.ParamByName('ptssor').AsString := AdoQuery.FieldByName('ptssor').AsString;
            AdsQuery.ParamByName('pthmor').AsString := AdoQuery.FieldByName('pthmor').AsString;
            AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
            AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
            AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
            AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
            AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
            AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
            AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
            AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
            AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
            AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
            AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
            AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
            AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
            AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
            AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
            AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
            AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
            AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
            AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
            AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
            AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
            AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
            AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
            AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
            AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
            AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
            AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
            AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
            AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
            AdsQuery.ParamByName('ptcptd').AsCurrency := AdoQuery.FieldByName('ptcptd').AsCurrency;
            AdsQuery.ParamByName('ptosts').AsString := AdoQuery.FieldByName('ptosts').AsString;
            AdsQuery.ParamByName('ptdtar').AsDateTime := AdoQuery.FieldByName('ptdtar').AsDateTime;
            AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
            AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
            AdsQuery.ParamByName('ptwsid').AsString := AdoQuery.FieldByName('ptwsid').AsString;
            AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
            AdsQuery.ParamByName('ptapdt').AsInteger := AdoQuery.FieldByName('ptapdt').AsInteger;
            AdsQuery.ParamByName('ptdtlc').AsInteger := AdoQuery.FieldByName('ptdtlc').AsInteger;
            AdsQuery.ParamByName('ptdtpi').AsInteger := AdoQuery.FieldByName('ptdtpi').AsInteger;
            AdsQuery.ParamByName('ptipty').AsString := AdoQuery.FieldByName('ptipty').AsString;
            AdsQuery.ParamByName('ptprty').AsString := AdoQuery.FieldByName('ptprty').AsString;
            AdsQuery.ParamByName('ptrnt').AsInteger := AdoQuery.FieldByName('ptrnt#').AsInteger;
            AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
            AdsQuery.ParamByName('ptcreate').AsDateTime := AdoQuery.FieldByName('ptcreate').AsDateTime;
            AdsQuery.ParamByName('ptcthold').AsDateTime := AdoQuery.FieldByName('ptcthold').AsDateTime;
            AdsQuery.ParamByName('ptauth1').AsString := AdoQuery.FieldByName('ptauth#').AsString;
            AdsQuery.ParamByName('ptauthid').AsString := AdoQuery.FieldByName('ptauthid').AsString;
            AdsQuery.ParamByName('ptdlvmth').AsInteger := AdoQuery.FieldByName('ptdlvmth').AsInteger;
            AdsQuery.ParamByName('ptshipseq').AsInteger := AdoQuery.FieldByName('ptshipseq').AsInteger;
            AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
            AdsQuery.ParamByName('pttaxgo').AsInteger := AdoQuery.FieldByName('pttaxgo').AsInteger;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
//          ScrapeCountTable('PDPPHDR', 'tmpPDPPHDR');
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPDPPHDR'')');
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure stgArkPDPPHDR()');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
//
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


