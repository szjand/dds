program xtime_appointments;

{$APPTYPE CONSOLE}
(*)
5/18/15
Jon;

The database change has been made on the Honda ADS server. The IP is 10.130.190.63. The username is Administrator and pwd is of the LS variety.

The database path is D:\Advantage\NewHondaService\NewHondaService.add

The ADS connection path is \\10.130.190.63:6363\Advantage\NewHondaService\NewHondaService.add

Users should get the new executables on restart of the app.

Thank you,
Ron
(*)
uses
  SysUtils,
  ActiveX,
  StrUtils,
  xtimeDM in 'xtimeDM.pas' {DM: TDataModule};

resourcestring
  executable = 'xtime_appointments';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
// *** active procs ***//
      DM.ext_xtime_appointments;
// *** active procs ***//
    except
      on E: Exception do
      begin
        DM.SendMail(executable + ' ' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
