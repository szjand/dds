CREATE TABLE zjon_SchedulerDetailAppointments ( 
      AppointmentID Char( 40 ),
      CreatedTS TimeStamp,
      CreatedBy CIChar( 40 ),
      StartTS TimeStamp,
      PromiseTS TimeStamp,
      Status CIChar( 25 ),
      RO CIChar( 10 ),
      Customer CIChar( 40 ),
      JobDescription Memo,
      ModelYear Integer,
      Make CIChar( 25 ),
      Model CIChar( 25 ),
      Color CIChar( 25 ),
      Vin CIChar( 17 ),
      Comments Memo,
      TechComments Memo) IN DATABASE;

INSERT INTO zjon_schedulerdetailappointments
SELECT * FROM schedulerdetailappointments

SELECT *
FROM zjon_schedulerdetailappointments

DELETE 
FROM zjon_schedulerdetailappointments


CREATE TABLE zjon_scheduler_detail_appointment_lines (
  appointment_id cichar(40),
  appointment_line_id cichar(40),
  comment memo);
  
CREATE TABLE scheduler_detail_appointment_lines (
  appointment_id cichar(40),
  appointment_line_id cichar(40),
  comment memo);  
  
  
  
SELECT *
FROM zjon_scheduler_detail_appointment_lines  

UPDATE zjon_schedulerdetailappointments
SET jobdescription = x.comment
FROM (
  SELECT *
  FROM zjon_scheduler_detail_appointment_lines) x
WHERE zjon_schedulerdetailappointments.appointmentid = x.appointmentid  