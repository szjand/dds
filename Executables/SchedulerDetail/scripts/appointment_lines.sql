/*

3/24/16
squawk now IN appointmentlines TABLE
AND
CONVERT to python

*/

-- orig query
SELECT a.appointmentid, a.created, 
  a.createdbyid,a.starttime, a.promisetime,
  a.status,a.ronumber,a.customer, 
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.modelcolor, a.vin,a.comments,  a.techcomments 
FROM appointments a 
WHERE advisorid = 'Detail' 
  AND CAST(starttime AS sql_date) >= curdate();

-- fucking timestamps
-- CAST AS sql_char seems to work
SELECT a.appointmentid, cast(a.created AS sql_char), 
  a.createdbyid, cast(a.starttime AS sql_char), 
  cast(a.promisetime AS sql_char),
  a.status,a.ronumber,a.customer, 
  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) AS squawk,
  a.modelyear, a.make, a.model,a.modelcolor, a.vin,a.comments,  a.techcomments 
FROM appointments a 
WHERE advisorid = 'Detail' 
  AND CAST(starttime AS sql_date) >= curdate();

-- fucking nulls returning AS NOT delimited string None
-- wtf
  
SELECT a.appointmentid, cast(a.created AS sql_char), 
  a.createdbyid, cast(a.starttime AS sql_char), 
  cast(a.promisetime AS sql_char),
  a.status,a.ronumber,a.customer, 
  CASE
    WHEN a.notes IS NULL THEN 'None'
	ELSE  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),'Customer States:','')) 
  END AS squawk,
  a.modelyear, a.make, a.model, a.modelcolor, a.vin,
  coalesce(a.comments,'None'),  
  coalesce(a.techcomments, 'None') 
FROM appointments a 
WHERE advisorid = 'Detail' 
  AND CAST(starttime AS sql_date) >= curdate();

SELECT * FROM appointments WHERE appointmentlineid = '{95F80073-1840-4051-913B-A20234FFBB72}'

SELECT * FROM appointmentlines WHERE appointmentlineid = '{95F80073-1840-4051-913B-A20234FFBB72}'

/**/
DROP TABLE xfm_appointment_lines;
CREATE TABLE xfm_appointment_lines (
  appointment_id cichar(40),
  appointment_line_id cichar(40),
  comment memo);
/**/
-- DELETE FROM xfm_appointment_lines;  
CREATE PROCEDURE xfm_detail_appointment_lines()
BEGIN 
/*
EXECUTE procedure xfm_detail_appointment_lines();
*/
DECLARE @line_id string;
DECLARE @appointment_id string;
DECLARE @comment string;  
DECLARE @cur_id cursor AS -- just detail appts for today AND beyond
  SELECT appointmentid, appointmentlineid
  FROM appointments 
  WHERE advisorid = 'Detail'
    AND CAST(starttime AS sql_date) >= curdate()
    AND appointmentlineid IS NOT NULL 
    AND appointmentlineid <> ''
  GROUP BY appointmentid, appointmentlineid;
DECLARE @cur_line CURSOR AS
  SELECT *
  FROM appointmentlines
  WHERE appointmentlineid = @cur_id.appointmentlineid;  
DELETE FROM xfm_appointment_lines;  
OPEN @cur_id;
TRY
  WHILE FETCH @cur_id DO
    @comment = ''; 
	@appointment_id = @cur_id.appointmentid;
    @line_id = @cur_id.appointmentlineid;
    OPEN @cur_line;
    TRY
      WHILE FETCH @cur_line DO
        @comment = 
          CASE
            WHEN length(TRIM(@comment)) = 0 THEN TRIM(@cur_line.concern)
            ELSE TRIM(@comment) + ', ' + TRIM(@cur_line.concern)
          END;
      END WHILE;
    FINALLY 
      CLOSE @cur_line;
    END TRY; 
    INSERT INTO xfm_appointment_lines values(@appointment_id, @line_id, @comment);             
  END WHILE;
FINALLY
  CLOSE @cur_id;
END TRY;  
END;   
-- SELECT * FROM xfm_appointment_lines;
  
