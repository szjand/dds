unit RoDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure SDPRHDR;        // 1
    procedure SDPRDET;        // 2
    procedure PDPPHDR;        // 3
    procedure PDPPDET;        // 4
    procedure PDPTDET;        // 5
    procedure SDPTECH;        // 6
    procedure stgRO;          // 7
    procedure maintDimTech;   // 8
    procedure rptTables;      // 9
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'ServiceLine';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.SDPRHDR;
var
  proc: string;
begin
  proc := 'SDPRHDR';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'delete from tmpSDPRHDR');
      PrepareQuery(AdsQuery, 'insert into tmpSDPRHDR (' +
          'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, ' +
          'PTARCK, PTPMTH, PTDATE, PTCDAT, PTFCDT, PTVIN, PTTEST, PTSVCO, ' +
          'PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, PTCHK#, ' +
          'PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, ' +
          'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
          'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST,  ' +
          'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
          'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, PTAPDT, ' +
          'PTDTCM, PTDTPI, PTTIIN, PTCREATE, PTDSPTEAM) ' +
          'values(' +
          ':PTCO, :PTRO, :PTRTYP, :PTWRO, :PTSWID, :PTRTCH, :PTCKEY, :PTCNAM, ' +
          ':PTARCK, :PTPMTH, :PTDATE, :PTCDAT, :PTFCDT, :PTVIN, :PTTEST, :PTSVCO, ' +
          ':PTSVCO2, :PTDEDA, :PTDEDA2, :PTWDED, :PTFRAN, :PTODOM, :PTMILO, :PTCHK, ' +
          ':PTPO, :PTREC, :PTPTOT, :PTLTOT, :PTSTOT, :PTDEDP, :PTSVCT, :PTSPOD, ' +
          ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
          ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, ' +
          ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
          ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTAUTH, :PTTAG, :PTAPDT, ' +
          ':PTDTCM, :PTDTPI, :PTTIIN, :PTCREATE, :PTDSPTEAM)');
        OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH,' +
            'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH, ' +
            'case ' +
            '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
            'end as PTDATE, ' +
            'case ' +
            '  when PTCDAT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) ' +
            'end as PTCDAT, ' +
            'case ' +
            '  when PTFCDT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) ' +
            'end as PTFCDT, ' +
            'PTVIN, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, ' +
            'PTCHK#, PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, PTCPHZ, ' +
            'PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, PTWAST3, PTWAST4, PTINST, ' +
            'PTINST2, PTINST3, PTINST4, PTSCST, PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, ' +
            'PTINSS, PTSCSS, PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, ' +
            'case ' +
            '  when PTAPDT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTAPDT), 4) || ''-'' || substr(digits(PTAPDT), 5, 2) || ''-'' || substr(digits(PTAPDT), 7, 2) as date) ' +
            'end as PTAPDT,  ' +
            'case ' +
            '  when PTDTCM = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDTCM), 4) || ''-'' || substr(digits(PTDTCM), 5, 2) || ''-'' || substr(digits(PTDTCM), 7, 2) as date) ' +
            'end as PTDTCM, ' +
            'case ' +
            '  when PTDTPI = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDTPI), 4) || ''-'' || substr(digits(PTDTPI), 5, 2) || ''-'' || substr(digits(PTDTPI), 7, 2) as date) ' +
            'end as PTDTPI, ' +
            'PTTIIN, ' +
            'case ' +
            '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
            '  else PTCREATE ' +
            'end as PTCREATE, ' +
            'PTDSPTEAM ' +
            'from rydedata.sdprhdr ' +
            'where ptco# in (''RY1'',''RY2'',''RY3'') ' +
            'and trim(ptro#) <> '''' ' +
            'and (  ' +
            '  cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
            '  or ( ' +
            '    ptcdat <> 0 ' +
            '    and  ' +
            '    cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
            '  or (' +
            '    ptfcdt <> 0 ' +
            '    and  ' +
            '    cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days))');

        while not AdoQuery.eof do
        begin
          AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
          AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
          AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
          AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
          AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
          AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
          AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
          AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
          AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
          AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
          AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
          AdsQuery.ParamByName('ptcdat').AsDate := AdoQuery.FieldByName('ptcdat').AsDateTime;
          AdsQuery.ParamByName('ptfcdt').AsDate := AdoQuery.FieldByName('ptfcdt').AsDateTime;
          AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
          AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
          AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
          AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
          AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
          AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
          AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
          AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
          AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
          AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
          AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
          AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
          AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
          AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
          AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
          AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
          AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
          AdsQuery.ParamByName('ptsvct').AsCurrency := AdoQuery.FieldByName('ptsvct').AsCurrency;
          AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
          AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
          AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
          AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
          AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
          AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
          AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
          AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
          AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
          AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
          AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
          AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
          AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
          AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
          AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
          AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
          AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
          AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
          AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
          AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
          AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
          AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
          AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
          AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
          AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
          AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
          AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
          AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
          AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
          AdsQuery.ParamByName('ptapdt').AsDate := AdoQuery.FieldByName('ptapdt').AsDateTime;
          AdsQuery.ParamByName('ptdtcm').AsDate := AdoQuery.FieldByName('ptdtcm').AsDateTime;
          AdsQuery.ParamByName('ptdtpi').AsDate := AdoQuery.FieldByName('ptdtpi').AsDateTime;
          AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
          AdsQuery.ParamByName('ptcreate').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('ptcreate').AsDateTime);
          AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
//      ScrapeCountQuery(' rydedata.sdprhdr ' +
//            'where ptco# in (''RY1'',''RY2'',''RY3'') ' +
//            'and trim(ptro#) <> '''' ' +
//            'and (  ' +
//            '  cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//            '  or ( ' +
//            '    ptcdat <> 0 ' +
//            '    and  ' +
//            '    cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//            '  or (' +
//            '    ptfcdt <> 0 ' +
//            '    and  ' +
//            '    cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days))',
//            ' tmpSDPRHDR');
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpSDPRHDR'')');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkSDPRHDR()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('SDPRHDR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;

end;

procedure TDM.SDPTECH;
var
  proc: string;
begin
  proc := 'SDPTECH';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaSDPTECH'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaSDPTECH(' +
          'STCO#, STTECH, STNAME, STEMP#, STLRAT, STPWRD, STPYEMP#, STACTV, STCRT#, STDFTSVCT) ' +
          'values(' +
          ':STCO, :STTECH, :STNAME, :STEMP, :STLRAT, :STPWRD, :STPYEMP, :STACTV, :STCRT, :STDFTSVCT)');
      OpenQuery(AdoQuery, 'select * from rydedata.sdptech');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('stco').AsString := AdoQuery.FieldByName('stco#').AsString;
        AdsQuery.ParamByName('sttech').AsString := AdoQuery.FieldByName('sttech').AsString;
        AdsQuery.ParamByName('stname').AsString := AdoQuery.FieldByName('stname').AsString;
        AdsQuery.ParamByName('stemp').AsString := AdoQuery.FieldByName('stemp#').AsString;
        AdsQuery.ParamByName('stlrat').AsCurrency := AdoQuery.FieldByName('stlrat').AsCurrency;
        AdsQuery.ParamByName('stpwrd').AsString := AdoQuery.FieldByName('stpwrd').AsString;
        AdsQuery.ParamByName('stpyemp').AsString := AdoQuery.FieldByName('stpyemp#').AsString;
        AdsQuery.ParamByName('stactv').AsString := AdoQuery.FieldByName('stactv').AsString;
        AdsQuery.ParamByName('stcrt').AsString := AdoQuery.FieldByName('stcrt#').AsString;
        AdsQuery.ParamByName('stdftsvct').AsString := AdoQuery.FieldByName('stdftsvct').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPTECH'')');
      ScrapeCountTable('sdptech', 'stgArkonaSDPTECH');
      DM.ExecuteQuery(AdsQuery, 'execute procedure xfmDimTech()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('sdptech.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.SDPRDET;
var
  proc: string;
begin
  proc := 'SDPRDET';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'delete from tmpSDPRDET');
      PrepareQuery(AdsQuery, 'insert into tmpSDPRDET values(' +
          ':PTCO, :PTRO, :PTLINE, :PTLTYP, :PTSEQ, :PTCODE, :PTDATE, :PTLPYM, ' +
          ':PTSVCTYP, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, :PTLHRS, :PTLAMT, ' +
          ':PTCOST, :PTARTF, :PTFAIL, :PTSLV, :PTSLI, :PTDCPN, :PTDBAS, ' +
          ':PTVATCODE, :PTVATAMT, :PTDISPTY1, :PTDISPTY2, :PTDISRANK1, ' +
          ':PTDISRANK2)');//, :PTPTAXAMT, :PTKTXAMT)');
      OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, ' +
          ' case ' +
          '   when ptdate = 0 then cast(''9999-12-31'' as date) ' +
          ' else ' +
          '   cast(left(digits(ptdate), 4) || ''-'' || substr(digits(ptdate), 5, 2) || ''-'' || substr(digits(ptdate), 7, 2) as date) ' +
          ' end as PTDATE, ' +
          ' PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF, ' +
          ' PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2, ' +
          ' PTDISRANK1, PTDISRANK2 ' + //, PTPTAXAMT, PTKTXAMT ' +
          ' from RYDEDATA.SDPRDET ' +
          ' where ptco# in (''RY1'', ''RY2'',''RY3'') ' + //and ptdbas <> ''V'' ' +
          '   and trim(ptro#) in (' +
          '     select trim(ptro#) from rydedata.sdprhdr ' +
          '     where ptco# in (''RY1'',''RY2'',''RY3'') ' +
          '       and trim(ptro#) <> '''' ' +
          '       and (  ' +
          '         cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
          '         or ( ' +
          '           ptcdat <> 0 ' +
          '           and  ' +
          '           cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
          '         or (' +
          '           ptfcdt <> 0 ' +
          '           and  ' +
          '           cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))');
      while not AdoQuery.eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
        AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
        AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
        AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
        AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
        if  AdoQuery.FieldByName('ptdate').AsString = '12/31/9999' then
          AdsQuery.ParamByName('ptdate').Clear
        else
          AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
        AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
        AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
        AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
        AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
        AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
        AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
        AdsQuery.ParamByName('ptlamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
        AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
        AdsQuery.ParamByName('ptartf').AsString := AdoQuery.FieldByName('ptartf').AsString;
        AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
        AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
        AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
        AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
        AdsQuery.ParamByName('ptdbas').AsString := AdoQuery.FieldByName('ptdbas').AsString;
        AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
        AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
        AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
        AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
        AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
        AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
//        AdsQuery.ParamByName('ptptaxamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
//        AdsQuery.ParamByName('ptktxamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end; //while not AdoQuery.eof do
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('tmpSDPRDET INSERT.  MESSAGE: ' + E.Message);
      end;
    end;
    try
//      ScrapeCountQuery(' rydedata.sdprdet ' +
//          ' where ptco# in (''RY1'', ''RY2'',''RY3'') ' + //and ptdbas <> ''V'' ' +
//          '   and trim(ptro#) in (' +
//          '     select trim(ptro#) from rydedata.sdprhdr ' +
//          '     where ptco# in (''RY1'',''RY2'',''RY3'') ' +
//          '       and trim(ptro#) <> '''' ' +
//          '       and (  ' +
//          '         cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days ' +
//          '         or ( ' +
//          '           ptcdat <> 0 ' +
//          '           and  ' +
//          '           cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) ' +
//          '         or (' +
//          '           ptfcdt <> 0 ' +
//          '           and  ' +
//          '           cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))',
//          ' tmpSDPRDET');
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpSDPRDET'')');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkSDPRDET()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('SDPRDET.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;

end;



procedure TDM.PDPPHDR;
var
  proc: string;
begin
  proc := 'PDPPHDR';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'delete from tmpPDPPHDR');
      PrepareQuery(AdsQuery, ' insert into tmpPDPPHDR(' +
          'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, PTDATE, PTDOC#, ' +
          'PTCUS#, PTCKEY, PTCNAM, PTPHON, PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
          'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
          'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
          'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
          'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
          'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
          'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
          'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
          'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
          'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
          'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
          'PTDTAR, PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
          'PTPRTY, PTRNT#, PTTIIN, PTCREATE, PTCTHOLD, PTAUTH#, PTAUTHID, ' +
          'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO) ' +
          'values(' +
          ':PTCO, :PTPKEY, :PTCPID, :PTSTAT, :PTDTYP, :PTTTYP, :PTDATE, :PTDOC, ' +
          ':PTCUS, :PTCKEY, :PTCNAM, :PTPHON, :PTSKEY, :PTPMTH, :PTSTYP, :PTPLVL, ' +
          ':PTTAXE, :PTPTOT, :PTSHPT, :PTSPOD, :PTSPDC, :PTSPDP, :PTSPDO, :PTSPOH, ' +
          ':PTSTAX, :PTSTAX2, :PTSTAX3, :PTSTAX4, :PTCPNM, :PTSRTK, :PTACTP, :PTCCAR, ' +
          ':PTPO, :PTREC, :PTODOC, :PTINTA, :PTRTYP, :PTRSTS, :PTWARO, :PTPAPRV, ' +
          ':PTSAPRV, :PTSWID, :PTRTCH, :PTTEST, :PTSVCO, :PTSVCO2, :PTDEDA, :PTDEDA2, ' +
          ':PTWDED, :PTVIN, :PTSTK, :PTTRCK, :PTFRAN, :PTODOM, :PTMILO, :PTRTIM, ' +
          ':PTTAG, :PTCHK, :PTHCMT, :PTSSOR, :PTHMOR, :PTLTOT, :PTSTOT, :PTDEDP, ' +
          ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
          ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, '  +
          ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
          ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTCPTD, :PTOSTS, ' +
          ':PTDTAR, :PTARCK, :PTWRO, :PTWSID, :PTAUTH, :PTAPDT, :PTDTLC, :PTDTPI, :PTIPTY, ' +
          ':PTPRTY, :PTRNT, :PTTIIN, :PTCREATE, :PTCTHOLD, :PTAUTH1, :PTAUTHID, ' +
          ':PTDLVMTH, :PTSHIPSEQ, :PTDSPTEAM, :PTTAXGO)');
      OpenQuery(AdoQuery, 'select ' +
          'PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, ' +
          'cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) as PTDATE, ' +
          'PTDOC#, ' +
          'PTCUS#, PTCKEY, PTCNAM, ' +
          'CASE  WHEN PTPHON < 0 then 0 else ptphon end as ptphon, ' +
          'PTSKEY, PTPMTH, PTSTYP, PTPLVL, ' +
          'PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH, ' +
          'PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR, ' +
          'PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV, ' +
          'PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, ' +
          'PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM, ' +
          'PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP, ' +
          'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
          'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST, ' +
          'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
          'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, ' +
          'case ' +
          '  when PTDTAR = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTDTAR), 4) || ''-'' || substr(digits(PTDTAR), 5, 2) || ''-'' || substr(digits(PTDTAR), 7, 2) as date) ' +
          'end as PTDTAR, ' +
          'PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, ' +
          'PTPRTY, PTRNT#, PTTIIN, ' +
          'case ' +
          '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCREATE ' +
          'end as PTCREATE, ' +
          'case ' +
          '  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCTHOLD ' +
          'end as PTCTHOLD, ' +
          'PTAUTH#, PTAUTHID, ' +
          'PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO ' +
          'from rydedata.pdpphdr');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
        AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
        AdsQuery.ParamByName('ptstat').AsString := AdoQuery.FieldByName('ptstat').AsString;
        AdsQuery.ParamByName('ptdtyp').AsString := AdoQuery.FieldByName('ptdtyp').AsString;
        AdsQuery.ParamByName('ptttyp').AsString := AdoQuery.FieldByName('ptttyp').AsString;
        AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptdoc').AsString := AdoQuery.FieldByName('ptdoc#').AsString;
        AdsQuery.ParamByName('ptcus').AsString := AdoQuery.FieldByName('ptcus#').AsString;
        AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
        AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
        AdsQuery.ParamByName('ptphon').AsString := AdoQuery.FieldByName('ptphon').AsString;
        AdsQuery.ParamByName('ptskey').AsInteger := AdoQuery.FieldByName('ptskey').AsInteger;
        AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
        AdsQuery.ParamByName('ptstyp').AsString := AdoQuery.FieldByName('ptstyp').AsString;
        AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
        AdsQuery.ParamByName('pttaxe').AsString := AdoQuery.FieldByName('pttaxe').AsString;
        AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
        AdsQuery.ParamByName('ptshpt').AsCurrency := AdoQuery.FieldByName('ptshpt').AsCurrency;
        AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
        AdsQuery.ParamByName('ptspdc').AsCurrency := AdoQuery.FieldByName('ptspdc').AsCurrency;
        AdsQuery.ParamByName('ptspdp').AsCurrency := AdoQuery.FieldByName('ptspdp').AsCurrency;
        AdsQuery.ParamByName('ptspdo').AsString := AdoQuery.FieldByName('ptspdo').AsString;
        AdsQuery.ParamByName('ptspoh').AsString := AdoQuery.FieldByName('ptspoh').AsString;
        AdsQuery.ParamByName('ptstax').AsCurrency := AdoQuery.FieldByName('ptstax').AsCurrency;
        AdsQuery.ParamByName('ptstax2').AsCurrency := AdoQuery.FieldByName('ptstax2').AsCurrency;
        AdsQuery.ParamByName('ptstax3').AsCurrency := AdoQuery.FieldByName('ptstax3').AsCurrency;
        AdsQuery.ParamByName('ptstax4').AsCurrency := AdoQuery.FieldByName('ptstax4').AsCurrency;
        AdsQuery.ParamByName('ptcpnm').AsString := AdoQuery.FieldByName('ptcpnm').AsString;
        AdsQuery.ParamByName('ptsrtk').AsString := AdoQuery.FieldByName('ptsrtk').AsString;
        AdsQuery.ParamByName('ptactp').AsString := AdoQuery.FieldByName('ptactp').AsString;
        AdsQuery.ParamByName('ptccar').AsString := AdoQuery.FieldByName('ptccar').AsString;
        AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
        AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
        AdsQuery.ParamByName('ptodoc').AsString := AdoQuery.FieldByName('ptodoc').AsString;
        AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
        AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
        AdsQuery.ParamByName('ptrsts').AsString := AdoQuery.FieldByName('ptrsts').AsString;
        AdsQuery.ParamByName('ptwaro').AsString := AdoQuery.FieldByName('ptwaro').AsString;
        AdsQuery.ParamByName('ptpaprv').AsString := AdoQuery.FieldByName('ptpaprv').AsString;
        AdsQuery.ParamByName('ptsaprv').AsString := AdoQuery.FieldByName('ptsaprv').AsString;
        AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
        AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
        AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
        AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
        AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
        AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
        AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
        AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
        AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
        AdsQuery.ParamByName('ptstk').AsString := AdoQuery.FieldByName('ptstk#').AsString;
        AdsQuery.ParamByName('pttrck').AsString := AdoQuery.FieldByName('pttrck').AsString;
        AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
        AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
        AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
        AdsQuery.ParamByName('ptrtim').AsFloat := AdoQuery.FieldByName('ptrtim').AsFloat;
        AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
        AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
        AdsQuery.ParamByName('pthcmt').AsString := AdoQuery.FieldByName('pthcmt').AsString;
        AdsQuery.ParamByName('ptssor').AsString := AdoQuery.FieldByName('ptssor').AsString;
        AdsQuery.ParamByName('pthmor').AsString := AdoQuery.FieldByName('pthmor').AsString;
        AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
        AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
        AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
        AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
        AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
        AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
        AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
        AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
        AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
        AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
        AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
        AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
        AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
        AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
        AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
        AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
        AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
        AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
        AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
        AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
        AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
        AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
        AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
        AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
        AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
        AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
        AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
        AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
        AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
        AdsQuery.ParamByName('ptcptd').AsCurrency := AdoQuery.FieldByName('ptcptd').AsCurrency;
        AdsQuery.ParamByName('ptosts').AsString := AdoQuery.FieldByName('ptosts').AsString;
        AdsQuery.ParamByName('ptdtar').AsDateTime := AdoQuery.FieldByName('ptdtar').AsDateTime;
        AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
        AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
        AdsQuery.ParamByName('ptwsid').AsString := AdoQuery.FieldByName('ptwsid').AsString;
        AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
        AdsQuery.ParamByName('ptapdt').AsInteger := AdoQuery.FieldByName('ptapdt').AsInteger;
        AdsQuery.ParamByName('ptdtlc').AsInteger := AdoQuery.FieldByName('ptdtlc').AsInteger;
        AdsQuery.ParamByName('ptdtpi').AsInteger := AdoQuery.FieldByName('ptdtpi').AsInteger;
        AdsQuery.ParamByName('ptipty').AsString := AdoQuery.FieldByName('ptipty').AsString;
        AdsQuery.ParamByName('ptprty').AsString := AdoQuery.FieldByName('ptprty').AsString;
        AdsQuery.ParamByName('ptrnt').AsInteger := AdoQuery.FieldByName('ptrnt#').AsInteger;
        AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
        AdsQuery.ParamByName('ptcreate').AsDateTime := AdoQuery.FieldByName('ptcreate').AsDateTime;
        AdsQuery.ParamByName('ptcthold').AsDateTime := AdoQuery.FieldByName('ptcthold').AsDateTime;
        AdsQuery.ParamByName('ptauth1').AsString := AdoQuery.FieldByName('ptauth#').AsString;
        AdsQuery.ParamByName('ptauthid').AsString := AdoQuery.FieldByName('ptauthid').AsString;
        AdsQuery.ParamByName('ptdlvmth').AsInteger := AdoQuery.FieldByName('ptdlvmth').AsInteger;
        AdsQuery.ParamByName('ptshipseq').AsInteger := AdoQuery.FieldByName('ptshipseq').AsInteger;
        AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
        AdsQuery.ParamByName('pttaxgo').AsInteger := AdoQuery.FieldByName('pttaxgo').AsInteger;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
//      ScrapeCountTable('PDPPHDR', 'tmpPDPPHDR');
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPDPPHDR'')');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPDPPHDR()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PDPPHDR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDM.PDPTDET;
var
  proc: string;
begin
  proc := 'PDPTDET';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpPDPTDET'')');
      PrepareQuery(AdsQuery, 'insert into tmpPDPTDET (' +
          'PTCO#, PTINV#, PTLINE, PTSEQ#, PTTGRP, PTCODE, PTSOEP, PTDATE, ' +
          'PTCDATE, PTCPID, PTMANF, PTPART, PTSGRP, PTQTY, PTCOST, PTLIST, ' +
          'PTNET, PTEPCDIFF, PTSPCD, PTORSO, PTPOVR, PTGPRC, PTXCLD, ' +
          'PTFPRT, PTRTRN, PTOHAT, PTVATCODE, PTVATAMT) ' +
          'values(' +
          ':PTCO, :PTINV, :PTLINE, :PTSEQ, :PTTGRP, :PTCODE, :PTSOEP, :PTDATE, ' +
          ':PTCDATE, :PTCPID, :PTMANF, :PTPART, :PTSGRP, :PTQTY, :PTCOST, :PTLIST, ' +
          ':PTNET, :PTEPCDIFF, :PTSPCD, :PTORSO, :PTPOVR, :PTGPRC, :PTXCLD, ' +
          ':PTFPRT, :PTRTRN, :PTOHAT, :PTVATCODE, :PTVATAMT)');
      OpenQuery(AdoQuery, 'select PTCO#, PTINV#, PTLINE, PTSEQ#, PTTGRP, PTCODE, PTSOEP, ' +
          'case ' +
          '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
          'end as PTDATE, ' +
          'case ' +
          '  when PTCDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTCDATE), 4) || ''-'' || substr(digits(PTCDATE), 5, 2) || ''-'' || substr(digits(PTCDATE), 7, 2) as date) ' +
          'end as PTCDATE, ' +
          'PTCPID, PTMANF, PTPART, PTSGRP, PTQTY, PTCOST, PTLIST, ' +
          'PTNET, PTEPCDIFF, PTSPCD, PTORSO, PTPOVR, PTGPRC, PTXCLD, ' +
          'PTFPRT, PTRTRN, PTOHAT, PTVATCODE, PTVATAMT ' +
          'from rydedata.pdptdet ' +
          'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptinv#) <> ''''' +
          '  and ptdate <> 0 ' +
          '  and (  ' +
          '    cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 30 days ' +
          '    or ( ' +
          '      ptcdate <> 0 ' +
          '      and  ' +
          '      cast(left(digits(PTCDATE), 4) || ''-'' || substr(digits(PTCDATE), 5, 2) || ''-'' || substr(digits(PTCDATE), 7, 2) as date) > curdate() - 30 days))');

      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptinv').AsString := AdoQuery.FieldByName('ptinv#').AsString;
        AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
        AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
        AdsQuery.ParamByName('pttgrp').AsString := AdoQuery.FieldByName('pttgrp').AsString;
        AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
        AdsQuery.ParamByName('ptsoep').AsString := AdoQuery.FieldByName('ptsoep').AsString;
        AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptcdate').AsDateTime := AdoQuery.FieldByName('ptcdate').AsDateTime;
        AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
        AdsQuery.ParamByName('ptmanf').AsString := AdoQuery.FieldByName('ptmanf').AsString;
        AdsQuery.ParamByName('ptpart').AsString := AdoQuery.FieldByName('ptpart').AsString;
        AdsQuery.ParamByName('ptsgrp').AsString := AdoQuery.FieldByName('ptsgrp').AsString;
        AdsQuery.ParamByName('ptqty').AsInteger := AdoQuery.FieldByName('ptqty').AsInteger;
        AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
        AdsQuery.ParamByName('ptlist').AsCurrency := AdoQuery.FieldByName('ptlist').AsCurrency;
        AdsQuery.ParamByName('ptnet').AsCurrency := AdoQuery.FieldByName('ptnet').AsCurrency;
        AdsQuery.ParamByName('ptepcdiff').AsCurrency := AdoQuery.FieldByName('ptepcdiff').AsCurrency;
        AdsQuery.ParamByName('ptspcd').AsString := AdoQuery.FieldByName('ptspcd').AsString;
        AdsQuery.ParamByName('ptorso').AsString := AdoQuery.FieldByName('ptorso').AsString;
        AdsQuery.ParamByName('ptpovr').AsString := AdoQuery.FieldByName('ptpovr').AsString;
        AdsQuery.ParamByName('ptgprc').AsString := AdoQuery.FieldByName('ptgprc').AsString;
        AdsQuery.ParamByName('ptxcld').AsString := AdoQuery.FieldByName('ptxcld').AsString;
        AdsQuery.ParamByName('ptfprt').AsString := AdoQuery.FieldByName('ptfprt').AsString;
        AdsQuery.ParamByName('ptrtrn').AsString := AdoQuery.FieldByName('ptrtrn').AsString;
        AdsQuery.ParamByName('ptohat').AsInteger := AdoQuery.FieldByName('ptohat').AsInteger;
        AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
        AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ScrapeCountQuery(' rydedata.pdptdet ' +
          'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptinv#) <> ''''' +
          '  and ptdate <> 0 ' +
          '  and (  ' +
          '    cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 30 days ' +
          '    or ( ' +
          '      ptcdate <> 0 ' +
          '      and  ' +
          '      cast(left(digits(PTCDATE), 4) || ''-'' || substr(digits(PTCDATE), 5, 2) || ''-'' || substr(digits(PTCDATE), 7, 2) as date) > curdate() - 30 days))',
          'tmpPDPTDET');
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPDPTDET'')');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPDPTDET()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PDPTDET.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.PDPPDET;
var
  proc: string;
begin
  proc := 'PDPPDET';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'delete from tmpPDPPDET');
      PrepareQuery(AdsQuery, ' insert into tmpPDPPDET(' +
          'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
          'PTDATE, ' +
          'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
          'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
          'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
          'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
          'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
          'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
          'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
          'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
          'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
          'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME) ' +
          'values(' +
          ':PTCO, :PTPKEY, :PTLINE, :PTLTYP, :PTSEQ, :PTCPID, :PTCODE, :PTSOEP, ' +
          ':PTDATE, :PTMANF, :PTPART, :PTPLNG, :PTSGRP, :PTBINL, :PTQTY, :PTCOST, ' +
          ':PTLIST, :PTTRAD, :PTFPRC, :PTNET, :PTBLIST, :PTMETH, :PTPCT, :PTBASE, ' +
          ':PTSPCD, :PTPOVR, :PTGPRC, :PTCORE, :PTADDP, :PTSPPC, :PTORSO, :PTXCLD, ' +
          ':PTCMNT, :PTLSTS, :PTSVCTYP, :PTLPYM, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, ' +
          ':PTLHRS, :PTLCHR, :PTARLA, :PTFAIL, :PTPLVL, :PTPSAS, :PTSLV, :PTSLI, ' +
          ':PTDCPN, :PTFACT, :PTACODE, :PTBAWC, :PTDTLC, :PTINTA, :PTLAUTH, :PTNTOT, ' +
          ':PTOAMT, :PTOHRS, :PTPMCS, :PTPMRT, :PTRPRP, :PTTXIN, :PTUPSL, :PTWCTP, ' +
          ':PTVATCODE, :PTVATAMT, :PTRTNINVF, :PTSKLLVL, :PTOEMRPLF, :PTCSTDIF, ' +
          ':PTOPTSEQ, :PTDISSTRZ, :PTDSCLBR, :PTDSCPRT, :PTPICKZNE, :PTDISPTY1, ' +
          ':PTDISPTY2, :PTDISRANK1, :PTDISRANK2, :PTDSPADATE, :PTDSPATIME, :PTDSPMDATE, :PTDSPMTIME)');
      OpenQuery(AdoQuery, 'select ' +
          'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
          'case ' +
          '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
          'end as PTDATE, ' +
          'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
          'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
          'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
          'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
          'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
          'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
          'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
          'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
          'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
          'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME ' +
          'from rydedata.pdppdet');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
        AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
        AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
        AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
        AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
        AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
        AdsQuery.ParamByName('ptsoep').AsString := AdoQuery.FieldByName('ptsoep').AsString;
        AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptmanf').AsString := AdoQuery.FieldByName('ptmanf').AsString;
        AdsQuery.ParamByName('ptpart').AsString := AdoQuery.FieldByName('ptpart').AsString;
        AdsQuery.ParamByName('ptplng').AsInteger := AdoQuery.FieldByName('ptplng').AsInteger;
        AdsQuery.ParamByName('ptsgrp').AsString := AdoQuery.FieldByName('ptsgrp').AsString;
        AdsQuery.ParamByName('ptbinl').AsString := AdoQuery.FieldByName('ptbinl').AsString;
        AdsQuery.ParamByName('ptqty').AsInteger := AdoQuery.FieldByName('ptqty').AsInteger;
        AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
        AdsQuery.ParamByName('ptlist').AsCurrency := AdoQuery.FieldByName('ptlist').AsCurrency;
        AdsQuery.ParamByName('pttrad').AsCurrency := AdoQuery.FieldByName('pttrad').AsCurrency;
        AdsQuery.ParamByName('ptfprc').AsCurrency := AdoQuery.FieldByName('ptfprc').AsCurrency;
        AdsQuery.ParamByName('ptnet').AsCurrency := AdoQuery.FieldByName('ptnet').AsCurrency;
        AdsQuery.ParamByName('ptblist').AsCurrency := AdoQuery.FieldByName('ptblist').AsCurrency;
        AdsQuery.ParamByName('ptmeth').AsString := AdoQuery.FieldByName('ptmeth').AsString;
        AdsQuery.ParamByName('ptpct').AsInteger := AdoQuery.FieldByName('ptpct').AsInteger;
        AdsQuery.ParamByName('ptbase').AsString := AdoQuery.FieldByName('ptbase').AsString;
        AdsQuery.ParamByName('ptspcd').AsString := AdoQuery.FieldByName('ptspcd').AsString;
        AdsQuery.ParamByName('ptpovr').AsString := AdoQuery.FieldByName('ptpovr').AsString;
        AdsQuery.ParamByName('ptgprc').AsString := AdoQuery.FieldByName('ptgprc').AsString;
        AdsQuery.ParamByName('ptcore').AsString := AdoQuery.FieldByName('ptcore').AsString;
        AdsQuery.ParamByName('ptaddp').AsString := AdoQuery.FieldByName('ptaddp').AsString;
        AdsQuery.ParamByName('ptsppc').AsInteger := AdoQuery.FieldByName('ptsppc').AsInteger;
        AdsQuery.ParamByName('ptorso').AsString := AdoQuery.FieldByName('ptorso').AsString;
        AdsQuery.ParamByName('ptxcld').AsString := AdoQuery.FieldByName('ptxcld').AsString;
        AdsQuery.ParamByName('ptcmnt').AsString := AdoQuery.FieldByName('ptcmnt').AsString;
        AdsQuery.ParamByName('ptlsts').AsString := AdoQuery.FieldByName('ptlsts').AsString;
        AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
        AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
        AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
        AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
        AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
        AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
        AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
        AdsQuery.ParamByName('ptlchr').AsFloat := AdoQuery.FieldByName('ptlchr').AsFloat;
        AdsQuery.ParamByName('ptarla').AsCurrency := AdoQuery.FieldByName('ptarla').AsCurrency;
        AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
        AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
        AdsQuery.ParamByName('ptpsas').AsInteger := AdoQuery.FieldByName('ptpsas').AsInteger;
        AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
        AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
        AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
        AdsQuery.ParamByName('ptfact').AsFloat := AdoQuery.FieldByName('ptfact').AsFloat;
        AdsQuery.ParamByName('ptacode').AsString := AdoQuery.FieldByName('ptacode').AsString;
        AdsQuery.ParamByName('ptbawc').AsString := AdoQuery.FieldByName('ptbawc').AsString;
        AdsQuery.ParamByName('ptdtlc').AsFloat := AdoQuery.FieldByName('ptdtlc').AsFloat;
        AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
        AdsQuery.ParamByName('ptlauth').AsString := AdoQuery.FieldByName('ptlauth').AsString;
        AdsQuery.ParamByName('ptntot').AsFloat := AdoQuery.FieldByName('ptntot').AsFloat;
        AdsQuery.ParamByName('ptoamt').AsFloat := AdoQuery.FieldByName('ptoamt').AsFloat;
        AdsQuery.ParamByName('ptohrs').AsFloat := AdoQuery.FieldByName('ptohrs').AsFloat;
        AdsQuery.ParamByName('ptpmcs').AsFloat := AdoQuery.FieldByName('ptpmcs').AsFloat;
        AdsQuery.ParamByName('ptpmrt').AsFloat := AdoQuery.FieldByName('ptpmrt').AsFloat;
        AdsQuery.ParamByName('ptrprp').AsString := AdoQuery.FieldByName('ptrprp').AsString;
        AdsQuery.ParamByName('pttxin').AsString := AdoQuery.FieldByName('pttxin').AsString;
        AdsQuery.ParamByName('ptupsl').AsString := AdoQuery.FieldByName('ptupsl').AsString;
        AdsQuery.ParamByName('ptwctp').AsString := AdoQuery.FieldByName('ptwctp').AsString;
        AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
        AdsQuery.ParamByName('ptvatamt').AsFloat := AdoQuery.FieldByName('ptvatamt').AsFloat;
        AdsQuery.ParamByName('ptrtninvf').AsString := AdoQuery.FieldByName('ptrtninvf').AsString;
        AdsQuery.ParamByName('ptskllvl').AsString := AdoQuery.FieldByName('ptskllvl').AsString;
        AdsQuery.ParamByName('ptoemrplf').AsString := AdoQuery.FieldByName('ptoemrplf').AsString;
        AdsQuery.ParamByName('ptcstdif').AsFloat := AdoQuery.FieldByName('ptcstdif$').AsFloat;
        AdsQuery.ParamByName('ptoptseq').AsInteger := AdoQuery.FieldByName('ptoptseq#').AsInteger;
        AdsQuery.ParamByName('ptdisstrz').AsString := ''; //AdoQuery.FieldByName('ptdisstrz').AsString;
        AdsQuery.ParamByName('ptdsclbr').AsFloat := AdoQuery.FieldByName('ptdsclbr$').AsFloat;
        AdsQuery.ParamByName('ptdscprt').AsFloat := AdoQuery.FieldByName('ptdscprt$').AsFloat;
        AdsQuery.ParamByName('ptpickzne').AsString := AdoQuery.FieldByName('ptpickzne').AsString;
        AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
        AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
        AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
        AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
        AdsQuery.ParamByName('ptdspadate').AsString := ''; //AdoQuery.FieldByName('ptdspadate').AsString;
        AdsQuery.ParamByName('ptdspatime').AsString := ''; //AdoQuery.FieldByName('ptdspatime').AsString;
        AdsQuery.ParamByName('ptdspmdate').AsString := ''; //AdoQuery.FieldByName('ptdspmdate').AsString;
        AdsQuery.ParamByName('ptdspmtime').AsString := ''; //AdoQuery.FieldByName('ptdspmtime').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
//      ScrapeCountTable('PDPPDET', 'tmpPDPPDET');
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPDPPDET'')');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPDPPDET()'); // deletes dups inserts into stg from tmp
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PDPPDET.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.stgRO;
var
  proc: string;
begin
  proc := 'stgRO';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure stgRO()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('stgRO.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;

procedure TDM.maintDimTech;
var
  proc: string;
  PassFail: string;
begin
  proc := 'maintDimTech';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      OpenQuery(AdsQuery, 'execute procedure maintDimTech()');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Fail' then
      begin
        sendmail('*** Maintenance Required ***', 'as indicated by ServiceLine.maintDimTech');
      end;
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('maintDimTech.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;

procedure TDM.rptTables;
var
  proc: string;
begin
  proc := 'rptTables';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure rptServiceLineRptTables()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('stgRO.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
  end;
end;
//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;



procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;


end.


