unit todaySchedulerAppointmentsDM;
(*)
5/2/14
  add customer, homePhone, bestPhoneToday, cellPhone to stgRydellServiceAppointments
(**)
interface

uses
    SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
    IdSMTPBase, IdSMTP, IdMessage,
    Dialogs;

type
  TDataModule1 = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure CloseQuery(query: TDataSet);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure OpenQuery(query: TDataSet; sql: string);
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    procedure SendMail(Subject: string; Body: string='');
    procedure Appointments;
  end;
var
  DM: TDataModule1;
//  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  RSAdsCon: TADSConnection;
//  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'stgRydellService';

implementation

{$R *.dfm}

{ TDataModule1 }





constructor TDataModule1.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  RSAdsCon := TADSConnection.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdsCon.Connect;
  RSAdsCon.ConnectPath := '\\10.130.196.83:6363\advantage\rydellservice\rydellservice.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva22';
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDataModule1.Destroy;
begin
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  RSAdsCon.Disconnect;
  FreeAndNil(RSAdsCon);
  inherited;
end;

procedure TDataModule1.Appointments;
var
  proc: string;
  RSQuery: TADSQuery;
begin
  proc := 'Appointments';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'execute procedure sp_zaptable(''stgRydellServiceAppointments'')');
      ExecuteQuery(AdsQuery, 'delete from todaySchedulerAppointments');
      PrepareQuery(AdsQuery, 'insert into todaySchedulerAppointments values ' +
          '(:AppointmentID, :CreatedTS, :CreatedBy, :AdvisorStatus, :RO, :VIN,' +
          ':AppointmentTS, :HeavyHours, :MaintHours, :DriveHours, :Team, :ArrivalTS,' +
          ':TOCReleaseTS, :TOCFinishTS, :WriterID, :customer, :homePhone, :bestPhoneToday, :cellPhone)');
      RSQuery := TADSQuery.Create(nil);
      RSQuery.AdsConnection := RSAdsCon;
      RSQuery.SQL.Clear;
      RSQuery.Close;
      RSQuery.SQL.Text := 'SELECT appointmentid, created, createdbyid, ' +
            'a.status, ronumber, vin, starttime, heavyduration, maintenanceduration, ' +
            'driveabilityduration, a.teamid, arrivaltime, tocreleasetime, ' +
            'tocfinishtime, b.advisornumber, customer, ' +
            'replace(replace(replace(HomePhone,''('',''''),'')'',''''),''-'','''') as homePhone, ' +
            'replace(replace(replace(BestPhoneToday,''('',''''),'')'',''''),''-'','''') as bestPhoneToday, ' +
            'replace(replace(replace(CellPhone,''('',''''),'')'',''''),''-'','''') as cellPhone ' +
            'FROM appointments a  ' +
            'LEFT JOIN advisors b ON a.advisorid = b.shortname ' +
            'WHERE CAST(starttime AS sql_date) = curdate()';
      RSQuery.Open;

      while not RSQuery.Eof do
      begin
        AdsQuery.ParamByName('AppointmentID').AsString := RSQuery.FieldByName('AppointmentID').AsString;
        AdsQuery.ParamByName('CreatedTS').AsDateTime := RSQuery.FieldByName('Created').AsDateTime;
        AdsQuery.ParamByName('CreatedBy').AsString := RSQuery.FieldByName('CreatedByID').AsString;
        AdsQuery.ParamByName('AdvisorStatus').AsString := RSQuery.FieldByName('status').AsString;
        AdsQuery.ParamByName('RO').AsString := RSQuery.FieldByName('ronumber').AsString;
        AdsQuery.ParamByName('VIN').AsString := RSQuery.FieldByName('vin').AsString;
        AdsQuery.ParamByName('AppointmentTS').AsDateTime := RSQuery.FieldByName('starttime').AsDateTime;
        AdsQuery.ParamByName('HeavyHours').AsFloat := RSQuery.FieldByName('heavyduration').AsFloat;
        AdsQuery.ParamByName('MaintHours').AsFloat := RSQuery.FieldByName('maintenanceduration').AsFloat;
        AdsQuery.ParamByName('DriveHours').AsFloat := RSQuery.FieldByName('driveabilityduration').AsFloat;
        AdsQuery.ParamByName('Team').AsString := RSQuery.FieldByName('Teamid').AsString;
        AdsQuery.ParamByName('ArrivalTS').AsDateTime := RSQuery.FieldByName('arrivaltime').AsDateTime;
        AdsQuery.ParamByName('TOCReleaseTS').AsDateTime := RSQuery.FieldByName('TOCReleaseTime').AsDateTime;
        AdsQuery.ParamByName('TOCFinishTS').AsDateTime := RSQuery.FieldByName('TOCFinishTime').AsDateTime;
        AdsQuery.ParamByName('WriterID').AsString := RSQuery.FieldByName('advisornumber').AsString;
        AdsQuery.ParamByName('customer').AsString := RSQuery.FieldByName('customer').AsString;
        AdsQuery.ParamByName('homePhone').AsString := RSQuery.FieldByName('homePhone').AsString;
        AdsQuery.ParamByName('bestPhoneToday').AsString := RSQuery.FieldByName('bestPhoneToday').AsString;
        AdsQuery.ParamByName('cellPhone').AsString := RSQuery.FieldByName('cellPhone').AsString;
        AdsQuery.ExecSQL;;
        RSQuery.Next;
      end;
//      ScrapeCountTable('GLPOPYR', 'stgArkonaGLPOPYR');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('Appointments.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(RSQuery);
  end;
end;



////////////////////////////////////////////////////////////////////////////////
procedure TDataModule1.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
    end;
end;

function TDataModule1.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

function TDataModule1.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

procedure TDataModule1.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
  end;
end;


procedure TDataModule1.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
  end;
end;

procedure TDataModule1.SendMail(Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;

procedure TDataModule1.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
//    AdoQuery.SQL.Clear;
//    AdoQuery.Close;
  end;
end;


end.
