program stgZapAndReload;
(*)
if app crashes prior to TDataModule1.Create, there is no error handling, because
the except block tries to call to a non existent DM
2/19
  add GLPCUST
8/20/15: bopvref has failed the last 3 nights, unable to open file
         changed from zap to delete
(**)

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  stgZRDM in 'stgZRDM.pas' {DataModule1: TDataModule};
resourcestring

  executable = 'stgZapAndReload';
begin
  try
    try
      CoInitialize(nil);
      DM := TDataModule1.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// current procs //
      DM.GLPOPYR;        // 1
      DM.FFPXREFDTA;     // 2
      DM.GLPMAST;        // 3
      DM.FSPagesLines;   // 4
      DM.GLPDEPT;        // 5
      //DM.GLPPOHD;        // 6  removed 4/13/13, not used (yet)
      //DM.PDPTHDR;        // 7  removed 4/13/13, takes 1/2 hour, not used (yet)
      DM.GLPCRHD;        // 9
      //DM.GLPCRDT;        // 10 removed 4/13/13, takes 1/4 hour, not used (yet)
      DM.GLPJRND;        // 12
//      DM.PYHSHDTA;       // 13 removed 8/21/16 being done in python as part of scoo
//      DM.PYCNTRL;        // 14 removed 4/13/13, failed on stored proc, not handling changes
      DM.PYDEDUCT;       // 15
      DM.SDPXTIM;        // 16
      DM.GLPCUST;        // 17
//      DM.BOPSLSS;        // 19  removed 2/16/14 becomes part of exe.dimSalesPerson
      DM.PYPCODES;       // 20
//      DM.SDPLOPC;      7/26 moved to dimOpCode
      DM.PYPCLKL;
//      DM.BOPMAST;      // 3/23/14 moved to exe
      DM.PYPRJOBD;
      DM.BOPVREF;
      DM.INPOPTF;
      DM.INPOPTD;
// current procs //
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('stgZapAndReload Failed.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
//    DM.SendMail('stgZapAndReload passed');
  finally
    FreeAndNil(DM);
  end;
end.
